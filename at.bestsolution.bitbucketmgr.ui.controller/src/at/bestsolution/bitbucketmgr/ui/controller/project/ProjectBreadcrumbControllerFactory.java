package at.bestsolution.bitbucketmgr.ui.controller.project;

import at.bestsolution.efxclipse.runtime.di.CachingContextFunction;

public class ProjectBreadcrumbControllerFactory extends CachingContextFunction {

	public ProjectBreadcrumbControllerFactory() {
		super(E4ProjectBreadcrumbController.class);
	}

}
