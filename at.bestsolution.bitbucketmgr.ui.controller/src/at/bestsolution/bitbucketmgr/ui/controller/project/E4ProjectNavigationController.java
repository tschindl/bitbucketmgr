package at.bestsolution.bitbucketmgr.ui.controller.project;

import java.util.HashMap;

import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainerElement;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import at.bestsolution.bitbucketmgr.ui.project.ProjectNavigationController;

public class E4ProjectNavigationController implements ProjectNavigationController {
	@Inject
	MPerspective perspective;
	
	@Inject
	EModelService modelService;
	
	@Inject
	MApplication application;
	
	@Inject
	MWindow window;
	
	private HashMap<String, MPartSashContainerElement> elements = new HashMap<>();
	
	@Override
	public void openBugtracking() {
		open("at.bestsolution.bitbucketmgr.app.part.bugtracking");
	}
	
	@Override
	public void openOverview() {
		open("at.bestsolution.bitbucketmgr.app.part.overview");
	}
	
	@Override
	public void openCommitHistory() {
		open("at.bestsolution.bitbucketmgr.app.part.commithistory");
	}
	
	@Override
	public void openSource() {
		open("at.bestsolution.bitbucketmgr.app.source");
	}

	private void open(String id) {
		MPartSashContainer container = (MPartSashContainer) modelService.find("at.bestsolution.bitbucketmgr.app.contentArea", perspective);
		
		MPartSashContainerElement element = container.getChildren().remove(0);
		elements.put(element.getElementId(), element);
		
		element = elements.get(id);
		
		if( element == null ) {
			element = (MPartSashContainerElement) modelService.cloneSnippet(application, id, window);	
		}
		
		if( element != null ) {
			container.getChildren().add(element);	
		}
	}
}
