package at.bestsolution.bitbucketmgr.ui.controller.project;

import javax.inject.Inject;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MInputPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.impl.BasicFactoryImpl;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.emf.common.util.URI;

import at.bestsolution.bitbucketmgr.model.bitbucketmgr.Project;
import at.bestsolution.bitbucketmgr.ui.project.ProjectExplorerViewController;
import at.bestsolution.bitbucketmgr.ui.services.EditorInput;
import at.bestsolution.bitbucketmgr.ui.services.EditorInputService;
import at.bestsolution.bitbucketmgr.ui.services.EditorService;
import at.bestsolution.bitbucketmgr.ui.services.EditorServices;

@SuppressWarnings("restriction")
public class E4ProjectExplorerViewController implements ProjectExplorerViewController {
	@Inject
	Project project;
	
	@Inject
	EditorServices editorServices;
	
	@Inject
	EModelService modelService;
	
	@Inject
	MPerspective perspective;
	
	@Inject
	EditorInputService editorInputService;
	
	@Inject
	EPartService partService;
	
	@Inject
	public E4ProjectExplorerViewController(Project project) {
		boolean flag = false;
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		for( IProject p : ws.getRoot().getProjects() ) {
			if( p.getName().equals(project.getName()) ) {
				flag = true;
				break;
			}
		}
		
		if( ! flag ) {
			IProgressMonitor monitor = new NullProgressMonitor();
			//FIXME Need to check out from Bitbucket!!!
			final IProject p = ws.getRoot().getProject(project.getName());
			final IProjectDescription pd = ws.newProjectDescription(project.getName());
			
			try {
				ws.run(new IWorkspaceRunnable() { 

					@Override
					public void run(IProgressMonitor monitor) throws CoreException {
						if( ! p.exists() ) {
							p.create(pd, monitor);
						}
						
						if( ! p.isOpen() ) {
							p.open(monitor);
						}
					} 
					
				}, monitor);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void openEditor(IFile resource) {
		URI uri = URI.createPlatformResourceURI("/" + resource.getProject().getName()+"/" + resource.getProjectRelativePath(), true);
		EditorInput input = editorInputService.createEditorInput(uri.toString());

		for( EditorService service : editorServices.getServices() ) {
			if( service.handlesInput(input) ) {
				createOrSelectEditor(uri, service);
				break;
			}
		}
	}
	
	private void createOrSelectEditor(URI uri, EditorService service) {
		String contributionUri = service.getEditorComponentUri();
		String resourceUri = uri.toString();
		
		for( MInputPart p : modelService.findElements(perspective, null, MInputPart.class, null) ) {
			if( resourceUri.equals(p.getInputURI()) && contributionUri.equals(p.getContributionURI()) ) {
				MElementContainer<MUIElement> container = p.getParent();
				container.setSelectedElement(p);
				return;
			}
		}
		
		MPartStack stack = (MPartStack) modelService.find("at.bestsolution.bitbucketmgr.app.editorstack", perspective);
		
		MInputPart part = BasicFactoryImpl.eINSTANCE.createInputPart();
		part.getTags().add(EPartService.REMOVE_ON_HIDE_TAG);
		part.setContributionURI(contributionUri);
		part.setCloseable(true);
		part.setInputURI(resourceUri);
		part.setLabel(uri.lastSegment());
		
		stack.getChildren().add(part);
		partService.activate(part);
	}
}