package at.bestsolution.bitbucketmgr.ui.controller.project;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;

import at.bestsolution.bitbucketmgr.model.bitbucketmgr.Project;
import at.bestsolution.bitbucketmgr.services.AuthentificationService.AuthToken;
import at.bestsolution.bitbucketmgr.services.RepositoryService;
import at.bestsolution.bitbucketmgr.services.model.Issue;
import at.bestsolution.bitbucketmgr.ui.project.ProjectIssuesView;
import at.bestsolution.bitbucketmgr.ui.project.ProjectIssuesViewController;
import at.bestsolution.efxclipse.runtime.core.Callback;
import at.bestsolution.efxclipse.runtime.di.Util;

public class E4ProjectIssuesViewController implements ProjectIssuesViewController {
	@Inject
	RepositoryService repoService;
	
	@Inject
	AuthToken authToken;
	
	@Inject
	Project project;
	
	ProjectIssuesView view;

	@Override
	public void connect(final ProjectIssuesView view) {
		this.view = view;
		repoService.getIssues(new NullProgressMonitor(), authToken, authToken.getUsername(), project.getName(), 
		Util.onFX(new Callback<List<Issue>>() {
			
			@Override
			public void call(List<Issue> value) {
				E4ProjectIssuesViewController.this.view.setIssues(value);
			}
		}),
		Util.onFX(new Callback<IStatus>() {

			@Override
			public void call(IStatus value) {
				// TODO Auto-generated method stub
				
			}
		})
		);
	}
}