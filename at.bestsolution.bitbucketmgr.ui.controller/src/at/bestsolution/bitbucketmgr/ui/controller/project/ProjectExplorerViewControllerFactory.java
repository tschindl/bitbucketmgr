package at.bestsolution.bitbucketmgr.ui.controller.project;

import at.bestsolution.efxclipse.runtime.di.CachingContextFunction;

public class ProjectExplorerViewControllerFactory extends CachingContextFunction {

	public ProjectExplorerViewControllerFactory() {
		super(E4ProjectExplorerViewController.class);
	}

}
