package at.bestsolution.bitbucketmgr.ui.controller.project;

import at.bestsolution.efxclipse.runtime.di.CachingContextFunction;

public class ProjectNavigationControllerFactory extends CachingContextFunction {

	public ProjectNavigationControllerFactory() {
		super(E4ProjectNavigationController.class);
	}

}
