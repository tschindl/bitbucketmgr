package at.bestsolution.bitbucketmgr.ui.controller.project;

import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import at.bestsolution.bitbucketmgr.ui.project.ProjectBreadcrumbController;

@SuppressWarnings("restriction")
public class E4ProjectBreadcrumbController implements ProjectBreadcrumbController {
	@Inject
	private EPartService partService;
	
	@Inject
	private EModelService modelService;
	
	@Inject
	private MWindow window;
	
	@Override
	public void openProjectList() {
		MPerspective p = (MPerspective) modelService.find("at.bestsolution.bitbucketmgr.app.perspective.overview", window);
		partService.switchPerspective(p);
	}
	
}