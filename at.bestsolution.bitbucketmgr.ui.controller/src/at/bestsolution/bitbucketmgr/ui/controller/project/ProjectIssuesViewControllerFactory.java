package at.bestsolution.bitbucketmgr.ui.controller.project;

import at.bestsolution.efxclipse.runtime.di.CachingContextFunction;

public class ProjectIssuesViewControllerFactory extends CachingContextFunction {

	public ProjectIssuesViewControllerFactory() {
		super(E4ProjectIssuesViewController.class);
	}

}
