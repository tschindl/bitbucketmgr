package at.bestsolution.bitbucketmgr.app.handler;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

@SuppressWarnings("restriction")
public class SaveHandler {
	
	@CanExecute
	boolean canSave(MPart part) {
		return part.isDirty();
	}
	
	@Execute
	void savePart(MPart part, IEclipseContext context) {
		System.err.println("SAVING ...");
		ContextInjectionFactory.invoke(part.getObject(), Persist.class, context);
	}
}
