package at.bestsolution.bitbucketmgr.app.handler;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import at.bestsolution.efxclipse.text.jface.ITextOperationTarget;
import at.bestsolution.efxclipse.text.jface.source.ISourceViewer;

@SuppressWarnings("restriction")
public class ContentAssistHandler {
	
	@Execute
	void execute(MPart part) {
		System.err.println("EXECUTING CONTENT ASSIST");
		IAdaptable adaptable = (IAdaptable) part.getObject();
		ITextOperationTarget target = (ITextOperationTarget) adaptable.getAdapter(ITextOperationTarget.class);
		if( target != null ) {
			target.doOperation(ISourceViewer.CONTENTASSIST_PROPOSALS);
		}
	}
}