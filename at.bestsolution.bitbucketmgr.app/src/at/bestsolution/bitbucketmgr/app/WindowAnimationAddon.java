package at.bestsolution.bitbucketmgr.app;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.annotation.PostConstruct;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;

import at.bestsolution.efxclipse.runtime.workbench.renderers.base.services.WindowTransitionService;
import at.bestsolution.efxclipse.runtime.workbench.renderers.base.services.WindowTransitionService.AnimationDelegate;

@SuppressWarnings("restriction")
public class WindowAnimationAddon {
	@PostConstruct
	void init(IEclipseContext context) {
		context.set(WindowTransitionService.class, new WindowTransitionServiceImpl());
	}
	
	static class FadeDelegate implements AnimationDelegate<Stage> {
		private double from;
		private double to;
		private boolean hide;
		
		public FadeDelegate(double from, double to, boolean hide) {
			this.from = from;
			this.to = to;
			this.hide = hide;
		}
		
		@Override
		public void animate(final Stage window) {
			window.setOpacity(from);
			final Timeline timeline = new Timeline();
			final KeyFrame kf = new KeyFrame(Duration.millis(1000), new KeyValue(window.opacityProperty(), to));
			timeline.getKeyFrames().add(kf);
			
			if( hide ) {
				timeline.setOnFinished(new EventHandler<ActionEvent>() {
					
					@Override
					public void handle(ActionEvent event) {
						window.hide();
					}
				});
			} else {
				window.show();
			}
			
			timeline.play();
		}
	}
	
	static class WindowTransitionServiceImpl implements WindowTransitionService<Stage> {

		@Override
		public AnimationDelegate<Stage> getShowDelegate(
				MWindow window) {
			return new FadeDelegate(0.0, 1.0, false);
		}

		@Override
		public AnimationDelegate<Stage> getHideDelegate(
				MWindow window) {
			return new FadeDelegate(1.0, 0.0, true);
		}
	}
}
