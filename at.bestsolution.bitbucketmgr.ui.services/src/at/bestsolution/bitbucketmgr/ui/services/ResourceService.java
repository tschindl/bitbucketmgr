package at.bestsolution.bitbucketmgr.ui.services;

import javafx.stage.Stage;

import org.eclipse.core.resources.IFolder;

public interface ResourceService {
	public String getName();
	public void createFile(IFolder parent, Stage stage);
}
