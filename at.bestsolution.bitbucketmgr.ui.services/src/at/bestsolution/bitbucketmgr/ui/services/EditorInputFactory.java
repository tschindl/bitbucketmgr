package at.bestsolution.bitbucketmgr.ui.services;

public interface EditorInputFactory {
	public boolean handlesUri(String uri);
	public EditorInput createEditorInput(String uri);
}
