package at.bestsolution.bitbucketmgr.ui.services;

import java.util.ArrayList;
import java.util.List;

public class EditorInputService {
	private List<EditorInputFactory> factories = new ArrayList<>();
	
	public void addEditorInputFactory(EditorInputFactory factory) {
		factories.add(factory);
	}
	
	public void removeEditorInputFactory(EditorInputFactory factory) {
		factories.remove(factory);
	}
	
	public EditorInput createEditorInput(String uri) {
		for( EditorInputFactory f : factories ) {
			if( f.handlesUri(uri) ) {
				return f.createEditorInput(uri);
			}
		}
		return null;
	}
}
