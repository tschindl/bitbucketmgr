package at.bestsolution.bitbucketmgr.ui.services;

public interface EditorService {
	public String getEditorComponentUri();
	public boolean handlesInput(EditorInput input);
}
