package at.bestsolution.bitbucketmgr.ui.services;

import java.util.ArrayList;
import java.util.List;

public class EditorServices {
	private List<EditorService> services = new ArrayList<>();
	
	public void addEditorService(EditorService service) {
		services.add(service);
	}
	
	public void removeEditorService(EditorService service) {
		services.remove(service);
	}
	
	public List<EditorService> getServices() {
		return services;
	}
}
