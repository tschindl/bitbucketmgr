package at.bestsolution.bitbucketmgr.ui.services;

import org.eclipse.core.resources.IProject;

public interface ProjectService {
	public String getName();
	public void createProject(IProject project);
}
