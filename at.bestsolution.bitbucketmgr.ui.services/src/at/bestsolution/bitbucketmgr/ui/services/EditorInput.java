package at.bestsolution.bitbucketmgr.ui.services;

import javafx.util.Callback;

public interface EditorInput {
	public void setDirty(boolean dirty);
	public void addDirtyChangeCallback(Callback<Boolean,Void> callback);
	public void removeDirtyChangeCallback(Callback<Boolean,Void> callback);
}
