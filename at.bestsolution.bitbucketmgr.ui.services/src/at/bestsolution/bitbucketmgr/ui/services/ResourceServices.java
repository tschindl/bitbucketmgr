package at.bestsolution.bitbucketmgr.ui.services;

import java.util.ArrayList;
import java.util.List;

public class ResourceServices {
	private List<ResourceService> services = new ArrayList<>();
	
	public void addResourceService(ResourceService service) {
		this.services.add(service);
	}
	
	public void removeResourceService(ResourceService service) {
		this.services.add(service);
	}
	
	public List<ResourceService> getServices() {
		return services;
	}
}
