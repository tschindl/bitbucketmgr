package at.bestsolution.bitbucketmgr.ui.services;

import java.util.ArrayList;
import java.util.List;

import javafx.util.Callback;

import org.eclipse.core.resources.IFile;

public class FileInput implements ResourceFileInput {
	private boolean dirty;
	private List<Callback<Boolean, Void>> dirtyCallbacks = new ArrayList<>();
	private IFile file;
	
	public FileInput(IFile file) {
		this.file = file;
	}
	
	@Override
	public IFile getFile() {
		return file;
	}

	@Override
	public void addDirtyChangeCallback(Callback<Boolean, Void> callback) {
		dirtyCallbacks.add(callback);
	}
	
	@Override
	public void removeDirtyChangeCallback(Callback<Boolean, Void> callback) {
		dirtyCallbacks.remove(callback);
	}
	
	@Override
	public void setDirty(boolean dirty) {
		if( dirty != this.dirty ) {
			this.dirty = dirty;
			for(Callback<Boolean, Void> c : dirtyCallbacks) {
				c.call(dirty);
			}
		}
	}
}
