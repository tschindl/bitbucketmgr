package at.bestsolution.bitbucketmgr.ui.services;

import java.util.ArrayList;
import java.util.List;

public class ProjectServices {
	private List<ProjectService> projectServices = new ArrayList<>();
	
	public void addProjectService(ProjectService projectService) {
		System.err.println("REGISTERING: " + projectService);
		projectServices.add(projectService);
	}
	
	public void removeProjectService(ProjectService projectService) {
		projectServices.remove(projectService);
	}
	
	public List<ProjectService> getServices() {
		return projectServices;
	}
}
