package at.bestsolution.bitbucketmgr.ui.services.impl;

import javafx.util.Callback;

import org.eclipse.e4.core.contexts.ContextFunction;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.ui.basic.MInputPart;

import at.bestsolution.bitbucketmgr.ui.services.EditorInput;
import at.bestsolution.bitbucketmgr.ui.services.EditorInputService;

@SuppressWarnings("restriction")
public class EditorContextFunction extends ContextFunction {

	@Override
	public Object compute(IEclipseContext context) {
		EditorInputService service = context.get(EditorInputService.class);
		final MInputPart p = context.get(MInputPart.class);
		final EditorInput input = service.createEditorInput(p.getInputURI());
		input.addDirtyChangeCallback(new Callback<Boolean, Void>() {
			
			@Override
			public Void call(Boolean value) {
				p.setDirty(value.booleanValue());
				return null;
			}
		});
		return input;
	}

}
