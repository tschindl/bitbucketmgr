package at.bestsolution.bitbucketmgr.ui.services.impl;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;

import at.bestsolution.bitbucketmgr.ui.services.EditorInput;
import at.bestsolution.bitbucketmgr.ui.services.EditorInputFactory;
import at.bestsolution.bitbucketmgr.ui.services.FileInput;

public class WorkspaceFileInputFactory implements EditorInputFactory {

	@Override
	public boolean handlesUri(String uri) {
		URI emfURI = URI.createURI(uri);
		return emfURI.isPlatformResource();
	}

	@Override
	public EditorInput createEditorInput(String uri) {
		URI emfURI = URI.createURI(uri);
		
		if( emfURI.isPlatformResource() ) {
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			IProject project = root.getProject(emfURI.segment(1));
			IPath p = new Path(emfURI.segment(2));
			for( int i = 3; i < emfURI.segmentCount(); i++ ) {
				p = p.append(emfURI.segment(i));
			}
			
			IFile file = project.getFile(p);
			if( file.exists() ) {
				return new FileInput(file);	
			}
		}
		
		return null;
	}
	
}
