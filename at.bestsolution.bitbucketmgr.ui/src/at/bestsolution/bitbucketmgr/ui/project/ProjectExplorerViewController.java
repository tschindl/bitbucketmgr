package at.bestsolution.bitbucketmgr.ui.project;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;

public interface ProjectExplorerViewController {
	public void openEditor(IFile resource);
}
