package at.bestsolution.bitbucketmgr.ui.project;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.IRuntimeClasspathEntry;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;

import at.bestsolution.bitbucketmgr.model.bitbucketmgr.Project;
import at.bestsolution.bitbucketmgr.ui.services.ProjectService;
import at.bestsolution.bitbucketmgr.ui.services.ProjectServices;
import at.bestsolution.bitbucketmgr.ui.services.ResourceService;
import at.bestsolution.bitbucketmgr.ui.services.ResourceServices;

public class ProjectExplorerView {
	private Map<IResource, LazyTreeItem> map = new HashMap<>();
	
	private Image projectImage;
	private Image packageImage;
	private Image srcFolderImage;
	private Image javaFileImage;
	private TreeView<IResource> view;
	
	@Inject
	Project project;
	
	@Inject
	ProjectExplorerViewController controller;
	
	@Inject
	ProjectServices projectServices;
	
	@Inject
	ResourceServices resourceServices;
	
	@Inject
	Stage stage;
	
	@PostConstruct
	void init(BorderPane p) {
		initImages();
		view = new TreeView<>(new LazyTreeItem(ResourcesPlugin.getWorkspace().getRoot()));
		view.setCellFactory(new Callback<TreeView<IResource>, TreeCell<IResource>>() {
			
			@Override
			public TreeCell<IResource> call(TreeView<IResource> param) {
				TreeCell<IResource> r = new TreeCell<IResource>() {
					@Override
					protected void updateItem(IResource item, boolean empty) {
						super.updateItem(item, empty);
						
						if( empty ) {
							setText(null);
							setGraphic(null);
						} else {
							setText(item.getName());
							if( item instanceof IProject ) {
								setGraphic(new ImageView(projectImage));
							} else if( item instanceof IFolder ) {
								if( "src".equals(item.getName()) ) {
									setGraphic(new ImageView(srcFolderImage));	
								} else {
									setGraphic(new ImageView(packageImage));
								}
							} else if( item instanceof IFile ) {
								if( item.getName().endsWith(".java") ) {
									setGraphic(new ImageView(javaFileImage));
								}
							}
						}
					}
				}; 
				return r;
			}
		});
		view.setShowRoot(false);
		view.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if( event.getClickCount() == 2 && view.getSelectionModel().getSelectedItem().getValue() instanceof IFile ) {
					controller.openEditor((IFile) view.getSelectionModel().getSelectedItem().getValue());
				}
			}
		});
		
		final ContextMenu menu = new ContextMenu();
		menu.getItems().add(new MenuItem());
		menu.setOnShowing(new EventHandler<WindowEvent>() {
			
			@Override
			public void handle(WindowEvent arg0) {
				menu.getItems().clear();
				setupMenu(menu);
			}
		});
		menu.setOnHidden(new EventHandler<WindowEvent>() {
			
			@Override
			public void handle(WindowEvent arg0) {
				menu.getItems().add(new MenuItem());
			}
		});
		
		view.setContextMenu(menu);
		
		p.setCenter(view);
		trackResourceChanges();	
	}
	
	void trackResourceChanges() {
		ResourcesPlugin.getWorkspace().addResourceChangeListener(new IResourceChangeListener() {
			
			@Override
			public void resourceChanged(IResourceChangeEvent event) {
				if (event.getType() == IResourceChangeEvent.POST_CHANGE) {
					try {
						event.getDelta().accept(new IResourceDeltaVisitor() {
							@Override
							public boolean visit(IResourceDelta delta)
									throws CoreException {
								if (delta.getKind() == IResourceDelta.ADDED) {
									LazyTreeItem item = map.get(delta.getResource().getParent());
									if( item != null ) {
										for( TreeItem<IResource> i : item.getChildren() ) {
											if( i.getValue().equals(delta.getResource()) ) {
												return true;
											}
										}
										
										item.getChildren().add(new LazyTreeItem(delta.getResource()));
									}
								}
								return true;
							}
						});
					} catch (CoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
	}
	
	void setupMenu(ContextMenu menu) {
		System.err.println(view.getSelectionModel().getSelectedItem().getValue());
		if( view.getSelectionModel().getSelectedItem().getValue() instanceof IProject ) {
			for( ProjectService service : projectServices.getServices() ) {
				final ProjectService tmp = service;
				MenuItem item = new MenuItem(service.getName());
				item.setOnAction(new EventHandler<ActionEvent>() {
					
					@Override
					public void handle(ActionEvent arg0) {
						tmp.createProject((IProject) view.getSelectionModel().getSelectedItem().getValue());
					}
				});
				menu.getItems().add(item);
			}	
		}
		
		if( view.getSelectionModel().getSelectedItem().getValue() instanceof IContainer ) {
			for( ResourceService service : resourceServices.getServices() ) {
				final ResourceService tmp = service;
				MenuItem item = new MenuItem(service.getName());
				item.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent arg0) {
						tmp.createFile((IFolder) view.getSelectionModel().getSelectedItem().getValue(), stage);
					}
				});
				menu.getItems().add(item);
			}	
		}
		
		if( view.getSelectionModel().getSelectedItem().getValue() instanceof IFile ) {
			MenuItem item = new MenuItem("Run");
			item.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {
					launchIt((IFile) view.getSelectionModel().getSelectedItem().getValue());
				}
			});
			menu.getItems().add(item);
		}
		
		
	}
	
	private void launchIt(IFile f) {
		ILaunchManager mgr = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType type = mgr
				.getLaunchConfigurationType(IJavaLaunchConfigurationConstants.ID_JAVA_APPLICATION);
		try {
			ILaunchConfiguration[] configurations = mgr
					.getLaunchConfigurations(type);
			for (int i = 0; i < configurations.length; i++) {
				ILaunchConfiguration configuration = configurations[i];
				if (configuration.getName().equals("Start Class")) {
					configuration.delete();
					break;
				}
			}
 
			ILaunchConfigurationWorkingCopy workingCopy = type.newInstance(
					null, "Start Class");
			IVMInstall jre = JavaRuntime.getDefaultVMInstall();
			workingCopy.setAttribute(
					IJavaLaunchConfigurationConstants.ATTR_VM_INSTALL_NAME,
					jre.getName());
			workingCopy.setAttribute(
					IJavaLaunchConfigurationConstants.ATTR_VM_INSTALL_TYPE, jre
							.getVMInstallType().getId());
			
			ICompilationUnit unit = (ICompilationUnit) JavaCore.create(f);
			workingCopy.setAttribute(
					IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME, unit.findPrimaryType().getFullyQualifiedName());

			IJavaProject jp = JavaCore.create(f.getProject());

			IRuntimeClasspathEntry pr = JavaRuntime
					.newDefaultProjectClasspathEntry(jp);
			IPath systemLibsPath = new Path(JavaRuntime.JRE_CONTAINER);
			IRuntimeClasspathEntry systemLibsEntry = JavaRuntime
					.newRuntimeContainerClasspathEntry(systemLibsPath,
							IRuntimeClasspathEntry.STANDARD_CLASSES);
			List<String> classpath = new ArrayList<String>();
			classpath.add(systemLibsEntry.getMemento());
			classpath.add(pr.getMemento());
			workingCopy
					.setAttribute(
							IJavaLaunchConfigurationConstants.ATTR_CLASSPATH,
							classpath);
			workingCopy.setAttribute(
					IJavaLaunchConfigurationConstants.ATTR_DEFAULT_CLASSPATH,
					false);
			ILaunchConfiguration configuration = workingCopy.doSave();
			ILaunch l = configuration.launch(ILaunchManager.RUN_MODE,
					new NullProgressMonitor());
			IProcess p = l.getProcesses()[0];
		} catch(Throwable t) {
			t.printStackTrace();
		}
	}
	
	class LazyTreeItem extends TreeItem<IResource> {
		private boolean childrenMaterialized;
		
		public LazyTreeItem(IResource resource) {
			setValue(resource);
			map.put(resource, this);
		}
		
		@Override
		public ObservableList<TreeItem<IResource>> getChildren() {
			return super.getChildren();
		}
		
		@Override
		public boolean isLeaf() {
			if( ! childrenMaterialized ) {
				materializeChildren();
			}
			return super.getChildren().isEmpty();
		}
		
		public void forceRefresh() {
			
		}
		
		private void materializeChildren() {
			childrenMaterialized = true;
			if( getValue() instanceof IContainer ) {
				IContainer c = (IContainer) getValue();
				try {
					IResource[] resources = c.members();
					List<LazyTreeItem> children = new ArrayList<LazyTreeItem>(resources.length);
					for( IResource r : resources ) {
						if( r.isDerived() || r.getName().startsWith(".") ) {
							continue;
						}
						children.add(new LazyTreeItem(r));
					}
					super.getChildren().setAll(children);
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	public IResource getCurrentResource() {
		TreeItem<IResource> item = view.getSelectionModel().getSelectedItem();
		if( item != null ) {
			return item.getValue();
		}
		
		return null;
	}
	
	private void initImages() {
		try(InputStream in = getClass().getClassLoader().getResourceAsStream("/icons/16/prj_obj.gif")) {
			projectImage = new Image(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try(InputStream in = getClass().getClassLoader().getResourceAsStream("/icons/16/package_obj.gif")) {
			packageImage = new Image(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try(InputStream in = getClass().getClassLoader().getResourceAsStream("/icons/16/packagefolder_obj.gif")) {
			srcFolderImage = new Image(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try(InputStream in = getClass().getClassLoader().getResourceAsStream("/icons/16/jcu_obj.gif")) {
			javaFileImage = new Image(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
