package at.bestsolution.bitbucketmgr.ui.project;

import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;

import javax.annotation.PostConstruct;

public class ProjectCommitHistory {
	@PostConstruct
	void init(BorderPane pane) {
		pane.setPadding(new Insets(5));
		
		TabPane tabPane = new TabPane();
		tabPane.setSide(Side.RIGHT);
		
		{
			Tab tab = new Tab();
			tab.setClosable(false);
			tab.setText("Most recent");
			tabPane.getTabs().add(tab);
		}
		
		{
			Tab tab = new Tab();
			tab.setText("Branches");
			tab.setClosable(false);
			tabPane.getTabs().add(tab);
		}
		
		pane.setCenter(tabPane);
	}
}
