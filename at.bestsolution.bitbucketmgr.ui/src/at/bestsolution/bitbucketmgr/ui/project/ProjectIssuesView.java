package at.bestsolution.bitbucketmgr.ui.project;


import java.util.List;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import at.bestsolution.bitbucketmgr.services.model.Issue;

public class ProjectIssuesView {
	@Inject
	ProjectIssuesViewController controller;
	private VBox issueContainer;
	
	@PostConstruct
	void init(BorderPane p) {
		p.setPadding(new Insets(5));
		
		StackPane stack = new StackPane();
		BorderPane container = new BorderPane();
		container.getStyleClass().add("issueView");
		container.setPadding(new Insets(5));
		
		Node toolbar = createToolbar();
		
		BorderPane.setMargin(toolbar, new Insets(0,0,10,0));
		
		container.setTop(toolbar);
		container.setCenter(createIssueList());
		
		stack.getChildren().add(container);
		
		p.setCenter(stack);
		
		controller.connect(this);
	}
	
	private Node createIssueList() {
		ScrollPane scroll = new ScrollPane();
		scroll.setFitToWidth(true);
		scroll.setStyle("-fx-background-color: inherit");
		issueContainer = new VBox(5);
		
		scroll.setContent(issueContainer);
		
		return scroll;
	}
	
	private Node createToolbar() {
		HBox t = new HBox();
		t.getStyleClass().add("issueArea");
		
		{
			Button b = new Button("Create Issue");
			t.getChildren().add(b);
		}
		
		{
			Region spacer = new Region();
			spacer.setPrefWidth(30);
			t.getChildren().add(spacer);
		}
		
		{
			Button b = new Button("Open");
			t.getChildren().add(b);
		}
		
		{
			Button b = new Button("My issues");
			t.getChildren().add(b);
		}
		
		{
			Button b = new Button("All");
			t.getChildren().add(b);
		}
		
		{
			Region spacer = new Region();
			HBox.setHgrow(spacer, Priority.ALWAYS);
			t.getChildren().add(spacer);
		}
		
		{
			//TODO Make a nice search widget
			TextField search = new TextField();
			search.setPromptText("Find issues");
			t.getChildren().add(search);
		}
		
		return t;
	}
	
	static class IssueItem {
		private BorderPane container;
		
		private BorderPane statusRegion;
		
		private final Issue issue;
		
		public IssueItem(Issue issue) {
			this.issue = issue;
		}
		
		public void init(VBox parent) {
			container = new BorderPane();
			container.setFocusTraversable(true);
			container.getStyleClass().add("issueItem");
			
			HBox data = new HBox();
			
			{
				statusRegion = new BorderPane();
				statusRegion.setPrefWidth(70);
				statusRegion.setMaxWidth(70);
				Label l = new Label(issue.getStatus().toUpperCase());
				l.getStyleClass().addAll("issueStatus", issue.getStatus());
				BorderPane.setAlignment(l, Pos.CENTER_LEFT);
				statusRegion.setLeft(l);
				
				data.getChildren().add(statusRegion);
			}
			
			{
				VBox box = new VBox();
				box.getStyleClass().add("issueInfoArea");
				box.getChildren().add(LabelBuilder.create().styleClass("issueTitle").text(issue.getTitle()).build());
				box.getChildren().add(LabelBuilder.create().styleClass("issueDescription").text(firstLine(issue.getDescription())).build());
				
				HBox.setHgrow(box, Priority.ALWAYS);
				data.getChildren().add(box);
			}
			
			container.setCenter(data);
			
			{
				HBox box = new HBox();
				box.getStyleClass().add("actions");
				
				{
					ImageView v = new ImageView();
					v.getStyleClass().add("task-accept");
					box.getChildren().add(v);
				}
				
				{
					ImageView v = new ImageView();
					v.getStyleClass().add("task-complete");
					box.getChildren().add(v);
				}
				
				{
					ImageView v = new ImageView();
					v.getStyleClass().add("task-reject");
					box.getChildren().add(v);
				}
				
				{
					ImageView v = new ImageView();
					v.getStyleClass().add("task-assign");
					box.getChildren().add(v);
				}
				
				
				container.setRight(box);
			}
			 
			
			parent.getChildren().add(container);
		}
	}
	
	private static String firstLine(String text) {
		return text.split("\r|\n")[0];
	}
	
	public static class App extends Application {

		@Override
		public void start(Stage primaryStage) throws Exception {
			BorderPane p = new BorderPane();
			ProjectIssuesView v = new ProjectIssuesView();
			v.init(p);
			
			Scene s = new Scene(p);
			s.getStylesheets().add("file:/Users/tomschindl/git/bitbucketmgr/at.bestsolution.bitbucketmgr.ui/css/default.css");
			primaryStage.setScene(s);
			primaryStage.setWidth(300);
			primaryStage.setHeight(300);
			primaryStage.show();
		}
		
	}
	
	public static void main(String[] args) {
		App.launch(App.class, args);
	}

	public void setIssues(List<Issue> value) {
		for( Issue i : value ) {
			IssueItem ii = new IssueItem(i);
			ii.init(issueContainer);
		}
	}
}
