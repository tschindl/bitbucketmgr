package at.bestsolution.bitbucketmgr.ui.project;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

public class ProjectNavigation {
	@Inject
	ProjectNavigationController controller;
	
	@PostConstruct
	void init(BorderPane pane) {
		VBox box = new VBox(10);
		box.setPadding(new Insets(5));
		
		{
			Label l = new Label();
			l.setGraphic(new ImageView(new Image(getClass().getClassLoader().getResource("/icons/64/home.png").toExternalForm())));
			l.setTooltip(new Tooltip("Overview"));
			l.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					controller.openOverview();
				}
			});
			box.getChildren().add(l);
		}
		
		{
			Label l = new Label();
			l.setGraphic(new ImageView(new Image(getClass().getClassLoader().getResource("/icons/64/sources_inactive.png").toExternalForm())));
			l.setTooltip(new Tooltip("Sources"));
			l.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent arg0) {
					controller.openSource();
				}
			});
			box.getChildren().add(l);
		}
		
		{
			Label l = new Label();
			l.setGraphic(new ImageView(new Image(getClass().getClassLoader().getResource("/icons/64/history_inactive.png").toExternalForm())));
			l.setTooltip(new Tooltip("Commit History"));
			l.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent arg0) {
					controller.openCommitHistory();
				}
			});
			box.getChildren().add(l);
		}
		
		{
			Label l = new Label();
			l.setGraphic(new ImageView(new Image(getClass().getClassLoader().getResource("/icons/64/tickets_inactive.png").toExternalForm())));
			l.setTooltip(new Tooltip("Bugtracking"));
			l.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					controller.openBugtracking();
				}
			});
			box.getChildren().add(l);
		}
		
		{
			Label l = new Label();
			l.setGraphic(new ImageView(new Image(getClass().getClassLoader().getResource("/icons/64/build_inactive.png").toExternalForm())));
			l.setTooltip(new Tooltip("CI Server"));
			box.getChildren().add(l);
		}
		
		{
			Label l = new Label();
			l.setGraphic(new ImageView(new Image(getClass().getClassLoader().getResource("/icons/64/stackoverflow_inactive.png").toExternalForm())));
			l.setTooltip(new Tooltip("Stackoverflow"));
			box.getChildren().add(l);
		}
		
		{
			Label l = new Label();
			l.setGraphic(new ImageView(new Image(getClass().getClassLoader().getResource("/icons/64/statistics_inactive.png").toExternalForm())));
			l.setTooltip(new Tooltip("Statistics"));
			box.getChildren().add(l);
		}
		
		{
			Label l = new Label();
			l.setGraphic(new ImageView(new Image(getClass().getClassLoader().getResource("/icons/64/settings_inactive.png").toExternalForm())));
			l.setTooltip(new Tooltip("Settings"));
			box.getChildren().add(l);
		}
		
		pane.setTop(box);
	}
}
