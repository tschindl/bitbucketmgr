package at.bestsolution.bitbucketmgr.ui.project;

public interface ProjectNavigationController {

	void openBugtracking();

	void openOverview();

	void openCommitHistory();
	
	void openSource();
}
