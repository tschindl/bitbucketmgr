package at.bestsolution.bitbucketmgr.ui.project;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import at.bestsolution.bitbucketmgr.model.bitbucketmgr.Project;

public class ProjectBreadcrumb {
	private final Project project;
	private final ProjectBreadcrumbController controller;
	
	@Inject
	public ProjectBreadcrumb(Project project, ProjectBreadcrumbController controller) {
		this.project = project;
		this.controller = controller;
	}
	
	@PostConstruct
	void init(BorderPane pane) {
		//TODO Control this via CSS
		pane.setPadding(new Insets(5));
//		Label l = new Label("BREADCRUMB");
//		BorderPane.setAlignment(l, Pos.CENTER_LEFT);
		HBox b = new HBox();
		b.getStyleClass().addAll("breadcrumbContainer");
		
		{
			Label l = new Label("/");
			b.getChildren().add(l);	
		}
		
		{
			Label l = new Label("Projects");
			l.getStyleClass().add("link");
			l.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					controller.openProjectList();
				}
			});
			b.getChildren().add(l);	
		}
		
		{
			Label l = new Label("/");
			b.getChildren().add(l);	
		}
		
		{
			Label l = new Label(project.getName());
			b.getChildren().add(l);	
		}
		
		pane.setCenter(b);
	}
}