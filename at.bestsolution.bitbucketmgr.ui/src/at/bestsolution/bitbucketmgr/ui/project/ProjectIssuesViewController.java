package at.bestsolution.bitbucketmgr.ui.project;

public interface ProjectIssuesViewController {
	public void connect(ProjectIssuesView view);
}
