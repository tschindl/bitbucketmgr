package at.bestsolution.bitbucketmgr.jdt.editor;

import at.bestsolution.bitbucketmgr.ui.services.EditorInput;
import at.bestsolution.bitbucketmgr.ui.services.EditorService;
import at.bestsolution.bitbucketmgr.ui.services.FileInput;

public class JavaEditorService implements EditorService {

	@Override
	public String getEditorComponentUri() {
		return "bundleclass://at.bestsolution.bitbucketmgr.jdt.editor/at.bestsolution.bitbucketmgr.jdt.editor.JavaEditor";
	}

	@Override
	public boolean handlesInput(EditorInput input) {
		return input instanceof FileInput;
	}

}
