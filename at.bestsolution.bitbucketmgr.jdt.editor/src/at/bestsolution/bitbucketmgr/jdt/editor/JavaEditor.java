package at.bestsolution.bitbucketmgr.jdt.editor;

import javafx.scene.layout.BorderPane;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.text.IDocument;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.JavaPlugin;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.ASTProvider;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.EditorUtility;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.ICompilationUnitDocumentProvider;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.JavaSourceViewer;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.SemanticHighlightingManager;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.IJavaPartitions;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.JavaSourceViewerConfiguration;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.JavaTextTools;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.IJavaReconcilingListener;
import at.bestsolution.bitbucketmgr.ui.services.EditorInput;
import at.bestsolution.bitbucketmgr.ui.services.FileInput;
import at.bestsolution.efxclipse.jface.preferences.IPreferenceStore;
import at.bestsolution.efxclipse.text.editor.FileEditorInput;
import at.bestsolution.efxclipse.text.editor.IDocumentProvider;
import at.bestsolution.efxclipse.text.editor.IEditorInput;
import at.bestsolution.efxclipse.text.editor.IElementStateListener;
import at.bestsolution.efxclipse.text.editor.ITextEditor;
import at.bestsolution.efxclipse.text.jface.ITextOperationTarget;

@SuppressWarnings("restriction")
public class JavaEditor implements ITextEditor, IJavaReconcilingListener {
	
	EditorInput input;
	ICompilationUnitDocumentProvider provider;
	IDocument document;
	IEditorInput editorInput;
	ElementStateListener listener;
	private SemanticHighlightingManager semanticManager;
	/**
	 * Reconciling listeners.
	 * @since 3.0
	 */
	private final ListenerList fReconcilingListeners= new ListenerList(ListenerList.IDENTITY);
	private JavaSourceViewer viewer;

	
	@Inject
	public JavaEditor(EditorInput input) {
		System.err.println("INPUT: " + input);
		this.input = input;
		listener = new ElementStateListener();
		provider = JavaPlugin.getDefault().getCompilationUnitDocumentProvider();
		provider.addElementStateListener(listener);
		editorInput = new FileEditorInput(((FileInput)input).getFile());
		try {
			provider.connect(editorInput);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document = provider.getDocument(editorInput);
	}
	
	@Override
	public IEditorInput getEditorInput() {
		return editorInput;
	}
	
	@Override
	public IDocumentProvider getDocumentProvider() {
		return provider;
	}
	
	@PostConstruct
	void init(BorderPane p) {
		viewer = new JavaSourceViewer();
		
//		IPreferenceStore store = JavaPlugin.getDefault().getPreferenceStore();
//		store.setValue(IJavaColorConstants.JAVA_KEYWORD, StringConverter.asString(RGB.iRGB(153,51,102)));
//		store.setValue(PreferenceConstants., StringConverter.asString(RGB.iRGB(153,51,102)));
		
		JavaTextTools textTools= JavaPlugin.getDefault().getJavaTextTools();
		
		viewer.configure(new JavaSourceViewerConfiguration(textTools.getColorManager(), JavaPlugin.getDefault().getPreferenceStore(), this, IJavaPartitions.JAVA_PARTITIONING));
		viewer.setDocument(document);
//		textTools.setupJavaDocumentPartitioner(document, IJavaPartitions.JAVA_PARTITIONING); 
		
		
//		if (fSemanticManager == null) {
			semanticManager= new SemanticHighlightingManager();
			semanticManager.install(this, (JavaSourceViewer) viewer, JavaPlugin.getDefault().getJavaTextTools().getColorManager(), getPreferenceStore());
//		}
		
		p.setCenter(viewer.getTextWidget());
		ASTProvider.getASTProvider().activeJavaEditorChanged(this);
	}
		
	@Focus
	void focused() {
		//FIXME This needs to be tracked by the ASTProvider itself!!
		ASTProvider.getASTProvider().activeJavaEditorChanged(this);
	}
	
	@Override
	public boolean isEditable() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@PreDestroy
	void dispose() {
		if( provider != null ) {
			provider.removeElementStateListener(listener);
//			try {
//				provider.resetDocument(editorInput);
//			} catch (CoreException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			provider.disconnect(editorInput);
		}
	}
	
	@Persist
	void save() {
		try {
			provider.saveDocument(new NullProgressMonitor(), editorInput, document, true);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	class ElementStateListener implements IElementStateListener {

		@Override
		public void elementDirtyStateChanged(Object element, boolean isDirty) {
			System.err.println("MODIFIED: " + element + " vs " + editorInput);
			if( element == editorInput ) {
				input.setDirty(isDirty);
			}
		}

		@Override
		public void elementContentAboutToBeReplaced(Object element) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void elementContentReplaced(Object element) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void elementDeleted(Object element) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void elementMoved(Object originalElement, Object movedElement) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * Returns the Java element wrapped by this editors input.
	 *
	 * @return the Java element wrapped by this editors input.
	 * @since 3.0
	 */
	public ITypeRoot getInputJavaElement() {
		return EditorUtility.getEditorInputJavaElement(this, false);
	}

	/**
	 * Returns a new Java source viewer configuration.
	 *
	 * @return a new <code>JavaSourceViewerConfiguration</code>
	 * @since 3.3
	 */
	public JavaSourceViewerConfiguration createJavaSourceViewerConfiguration() {
		JavaTextTools textTools= JavaPlugin.getDefault().getJavaTextTools();
		return new JavaSourceViewerConfiguration(textTools.getColorManager(), getPreferenceStore(), this, IJavaPartitions.JAVA_PARTITIONING);
	}

	private IPreferenceStore getPreferenceStore() {
		return JavaPlugin.getDefault().getPreferenceStore();
	}

	/*
	 * @see org.eclipse.jdt.internal.ui.text.java.IJavaReconcilingListener#aboutToBeReconciled()
	 * @since 3.0
	 */
	public void aboutToBeReconciled() {

		// Notify AST provider
		JavaPlugin.getDefault().getASTProvider().aboutToBeReconciled(getInputJavaElement());

		// Notify listeners
		Object[] listeners = fReconcilingListeners.getListeners();
		for (int i = 0, length= listeners.length; i < length; ++i)
			((IJavaReconcilingListener)listeners[i]).aboutToBeReconciled();
	}

	/*
	 * @see org.eclipse.jdt.internal.ui.text.java.IJavaReconcilingListener#reconciled(CompilationUnit, boolean, IProgressMonitor)
	 * @since 3.0
	 */
	public void reconciled(CompilationUnit ast, boolean forced, IProgressMonitor progressMonitor) {

		// see https://bugs.eclipse.org/bugs/show_bug.cgi?id=58245
		JavaPlugin javaPlugin= JavaPlugin.getDefault();
		if (javaPlugin == null)
			return;

		// Always notify AST provider
		javaPlugin.getASTProvider().reconciled(ast, getInputJavaElement(), progressMonitor);

		// Notify listeners
		Object[] listeners = fReconcilingListeners.getListeners();
		for (int i = 0, length= listeners.length; i < length; ++i)
			((IJavaReconcilingListener)listeners[i]).reconciled(ast, forced, progressMonitor);

//		// Update Java Outline page selection
//		if (!forced && !progressMonitor.isCanceled()) {
//			Shell shell= getSite().getShell();
//			if (shell != null && !shell.isDisposed()) {
//				shell.getDisplay().asyncExec(new Runnable() {
//					public void run() {
//						selectionChanged();
//					}
//				});
//			}
//		}
	}
	
	/**
	 * Adds the given listener.
	 * Has no effect if an identical listener was not already registered.
	 *
	 * @param listener	The reconcile listener to be added
	 * @since 3.0
	 */
	public final void addReconcileListener(IJavaReconcilingListener listener) {
		synchronized (fReconcilingListeners) {
			fReconcilingListeners.add(listener);
		}
	}
	

	/**
	 * Removes the given listener.
	 * Has no effect if an identical listener was not already registered.
	 *
	 * @param listener	the reconcile listener to be removed
	 * @since 3.0
	 */
	public final void removeReconcileListener(IJavaReconcilingListener listener) {
		synchronized (fReconcilingListeners) {
			fReconcilingListeners.remove(listener);
		}
	}
	
	@Override
	public Object getAdapter(Class adapter) {
		if( adapter == ITextOperationTarget.class ) {
			return viewer;
		}
		return null;
	}
}
