package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text;

import org.eclipse.jface.text.IDocument;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.ContentAssistProcessor;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.JavaCodeScanner;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.JavaCompletionProcessor;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.javadoc.JavaDocScanner;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.javadoc.JavadocCompletionProcessor;
import at.bestsolution.efxclipse.jface.preferences.IPreferenceStore;
import at.bestsolution.efxclipse.jface.util.PropertyChangeEvent;
import at.bestsolution.efxclipse.text.editor.ITextEditor;
import at.bestsolution.efxclipse.text.editor.TextSourceViewerConfiguration;
import at.bestsolution.efxclipse.text.jface.contentassist.ContentAssistant;
import at.bestsolution.efxclipse.text.jface.contentassist.IContentAssistProcessor;
import at.bestsolution.efxclipse.text.jface.contentassist.IContentAssistant;
import at.bestsolution.efxclipse.text.jface.presentation.IPresentationReconciler;
import at.bestsolution.efxclipse.text.jface.presentation.PresentationReconciler;
import at.bestsolution.efxclipse.text.jface.reconciler.IReconciler;
import at.bestsolution.efxclipse.text.jface.rules.DefaultDamagerRepairer;
import at.bestsolution.efxclipse.text.jface.rules.RuleBasedScanner;
import at.bestsolution.efxclipse.text.jface.source.ISourceViewer;

public class JavaSourceViewerConfiguration extends TextSourceViewerConfiguration {
	private IColorManager colorManager;
	private ITextEditor textEditor;
	private String documentPartitioning;
	private JavaCodeScanner codeScanner;
	private JavaCommentScanner multilineCommentScanner;
	private JavaCommentScanner singlelineCommentScanner;
	private SingleTokenJavaScanner stringScanner;
	private JavaDocScanner javaDocScanner;

	public JavaSourceViewerConfiguration(IColorManager colorManager, IPreferenceStore preferenceStore, ITextEditor editor, String partitioning) {
		super(preferenceStore);
		this.colorManager= colorManager;
		this.textEditor= editor;
		this.documentPartitioning= partitioning;
		initializeScanners();
	}
	
	/**
	 * Initializes the scanners.
	 *
	 * @since 3.0
	 */
	private void initializeScanners() {
		this.codeScanner= new JavaCodeScanner(getColorManager(), preferenceStore);
		this.multilineCommentScanner= new JavaCommentScanner(getColorManager(), preferenceStore, IJavaColorConstants.JAVA_MULTI_LINE_COMMENT);
		this.singlelineCommentScanner= new JavaCommentScanner(getColorManager(), preferenceStore, IJavaColorConstants.JAVA_SINGLE_LINE_COMMENT);
		this.stringScanner= new SingleTokenJavaScanner(getColorManager(), preferenceStore, IJavaColorConstants.JAVA_STRING);
		this.javaDocScanner= new JavaDocScanner(getColorManager(), preferenceStore);
	}
	
	/*
	 * @see SourceViewerConfiguration#getPresentationReconciler(ISourceViewer)
	 */
	@Override
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {

		PresentationReconciler reconciler= new JavaPresentationReconciler();
		reconciler.setDocumentPartitioning(getConfiguredDocumentPartitioning(sourceViewer));

		DefaultDamagerRepairer dr= new DefaultDamagerRepairer(getCodeScanner());
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		dr= new DefaultDamagerRepairer(getJavaDocScanner());
		reconciler.setDamager(dr, IJavaPartitions.JAVA_DOC);
		reconciler.setRepairer(dr, IJavaPartitions.JAVA_DOC);

		dr= new DefaultDamagerRepairer(getMultilineCommentScanner());
		reconciler.setDamager(dr, IJavaPartitions.JAVA_MULTI_LINE_COMMENT);
		reconciler.setRepairer(dr, IJavaPartitions.JAVA_MULTI_LINE_COMMENT);

		dr= new DefaultDamagerRepairer(getSinglelineCommentScanner());
		reconciler.setDamager(dr, IJavaPartitions.JAVA_SINGLE_LINE_COMMENT);
		reconciler.setRepairer(dr, IJavaPartitions.JAVA_SINGLE_LINE_COMMENT);

		dr= new DefaultDamagerRepairer(getStringScanner());
		reconciler.setDamager(dr, IJavaPartitions.JAVA_STRING);
		reconciler.setRepairer(dr, IJavaPartitions.JAVA_STRING);

		dr= new DefaultDamagerRepairer(getStringScanner());
		reconciler.setDamager(dr, IJavaPartitions.JAVA_CHARACTER);
		reconciler.setRepairer(dr, IJavaPartitions.JAVA_CHARACTER);


		return reconciler;
	}
	
	/**
	 * Returns the JavaDoc scanner for this configuration.
	 *
	 * @return the JavaDoc scanner
	 */
	protected RuleBasedScanner getJavaDocScanner() {
		return javaDocScanner;
	}
	
	/**
	 * Returns the Java source code scanner for this configuration.
	 *
	 * @return the Java source code scanner
	 */
	protected RuleBasedScanner getCodeScanner() {
		return codeScanner;
	}
	
	/**
	 * Returns the Java multi-line comment scanner for this configuration.
	 *
	 * @return the Java multi-line comment scanner
	 * @since 2.0
	 */
	protected RuleBasedScanner getMultilineCommentScanner() {
		return multilineCommentScanner;
	}

	/**
	 * Returns the Java single-line comment scanner for this configuration.
	 *
	 * @return the Java single-line comment scanner
	 * @since 2.0
	 */
	protected RuleBasedScanner getSinglelineCommentScanner() {
		return singlelineCommentScanner;
	}
	
	/**
	 * Returns the Java string scanner for this configuration.
	 *
	 * @return the Java string scanner
	 * @since 2.0
	 */
	protected RuleBasedScanner getStringScanner() {
		return stringScanner;
	}
	
	/**
	 * Returns the color manager for this configuration.
	 *
	 * @return the color manager
	 */
	protected IColorManager getColorManager() {
		return colorManager;
	}
	
	/*
	 * @see SourceViewerConfiguration#getConfiguredContentTypes(ISourceViewer)
	 */
	@Override
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		return new String[] {
			IDocument.DEFAULT_CONTENT_TYPE,
			IJavaPartitions.JAVA_DOC,
			IJavaPartitions.JAVA_MULTI_LINE_COMMENT,
			IJavaPartitions.JAVA_SINGLE_LINE_COMMENT,
			IJavaPartitions.JAVA_STRING,
			IJavaPartitions.JAVA_CHARACTER
		};
	}
	
	@Override
	public String getConfiguredDocumentPartitioning(ISourceViewer sourceViewer) {
		if (documentPartitioning != null)
			return documentPartitioning;
		return super.getConfiguredDocumentPartitioning(sourceViewer);
	}
	
	/*
	 * @see SourceViewerConfiguration#getReconciler(ISourceViewer)
	 */
	@Override
	public IReconciler getReconciler(ISourceViewer sourceViewer) {

		final ITextEditor editor= getEditor();
		if (editor != null && editor.isEditable()) {

			JavaCompositeReconcilingStrategy strategy= new JavaCompositeReconcilingStrategy(sourceViewer, editor, getConfiguredDocumentPartitioning(sourceViewer));
			JavaReconciler reconciler= new JavaReconciler(editor, strategy, false);
			reconciler.setIsAllowedToModifyDocument(false);
			reconciler.setDelay(500);

			return reconciler;
		}
		return null;
	}
	
	/**
	 * Returns the editor in which the configured viewer(s) will reside.
	 *
	 * @return the enclosing editor
	 */
	protected ITextEditor getEditor() {
		return textEditor;
	}

	public void handlePropertyChangeEvent(PropertyChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
		if (getEditor() != null) {
			ContentAssistant assistant= new ContentAssistant();
			assistant.setDocumentPartitioning(getConfiguredDocumentPartitioning(sourceViewer));
			
			IContentAssistProcessor javaProcessor= new JavaCompletionProcessor(getEditor(), assistant, IDocument.DEFAULT_CONTENT_TYPE);
			assistant.setContentAssistProcessor(javaProcessor, IDocument.DEFAULT_CONTENT_TYPE);

			ContentAssistProcessor singleLineProcessor= new JavaCompletionProcessor(getEditor(), assistant, IJavaPartitions.JAVA_SINGLE_LINE_COMMENT);
			assistant.setContentAssistProcessor(singleLineProcessor, IJavaPartitions.JAVA_SINGLE_LINE_COMMENT);

			ContentAssistProcessor stringProcessor= new JavaCompletionProcessor(getEditor(), assistant, IJavaPartitions.JAVA_STRING);
			assistant.setContentAssistProcessor(stringProcessor, IJavaPartitions.JAVA_STRING);

			ContentAssistProcessor multiLineProcessor= new JavaCompletionProcessor(getEditor(), assistant, IJavaPartitions.JAVA_MULTI_LINE_COMMENT);
			assistant.setContentAssistProcessor(multiLineProcessor, IJavaPartitions.JAVA_MULTI_LINE_COMMENT);

			ContentAssistProcessor javadocProcessor= new JavadocCompletionProcessor(getEditor(), assistant);
			assistant.setContentAssistProcessor(javadocProcessor, IJavaPartitions.JAVA_DOC);

//			ContentAssistPreference.configure(assistant, fPreferenceStore);
//
//			assistant.setContextInformationPopupOrientation(IContentAssistant.CONTEXT_INFO_ABOVE);
//			assistant.setInformationControlCreator(new IInformationControlCreator() {
//				public IInformationControl createInformationControl(Shell parent) {
//					return new DefaultInformationControl(parent, JavaPlugin.getAdditionalInfoAffordanceString());
//				}
//			});
			
			return assistant;
		}
		
		return null;
	}
}
