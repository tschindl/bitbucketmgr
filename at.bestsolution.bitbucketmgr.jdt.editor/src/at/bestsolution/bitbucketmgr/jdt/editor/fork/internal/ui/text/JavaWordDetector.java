package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text;

import at.bestsolution.efxclipse.text.jface.rules.IWordDetector;


/**
 * A Java aware word detector.
 */
public class JavaWordDetector implements IWordDetector {

	/*
	 * @see IWordDetector#isWordStart
	 */
	public boolean isWordStart(char c) {
		return Character.isJavaIdentifierStart(c);
	}

	/*
	 * @see IWordDetector#isWordPart
	 */
	public boolean isWordPart(char c) {
		return Character.isJavaIdentifierPart(c);
	}
}
