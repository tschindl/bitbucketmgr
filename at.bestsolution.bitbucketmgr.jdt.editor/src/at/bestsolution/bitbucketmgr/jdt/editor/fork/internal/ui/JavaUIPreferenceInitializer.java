package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;

import at.bestsolution.efxclipse.jface.RGB;
import at.bestsolution.efxclipse.jface.preferences.IPreferenceStore;
import at.bestsolution.efxclipse.jface.preferences.PreferenceConverter;

public class JavaUIPreferenceInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {
		System.err.println("==================> RUNNING!!!!!");
		IPreferenceStore store = PreferenceConstants.getPreferenceStore();

//		EditorsUI.useAnnotationsPreferencePage(store);
//		EditorsUI.useQuickDiffPreferencePage(store);
		PreferenceConstants.initializeDefaultValues(store);
	}

	public static void setThemeBasedPreferences(IPreferenceStore store, boolean fireEvent) {
		setDefault(
				store,
				PreferenceConstants.EDITOR_STRING_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_STRING_COLOR, new RGB(42, 0, 255)), fireEvent);
				RGB.iRGB(42, 0, 255),fireEvent);
		setDefault(
				store,
				PreferenceConstants.EDITOR_JAVA_DEFAULT_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_JAVA_DEFAULT_COLOR, new RGB(0, 0, 0)), fireEvent);
				RGB.iRGB(0, 0, 0),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_MULTI_LINE_COMMENT_COLOR, 
//				findRGB(registry, IJavaThemeConstants.EDITOR_MULTI_LINE_COMMENT_COLOR, new RGB(63, 127, 95)), fireEvent);
				RGB.iRGB(63, 127, 95),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_SINGLE_LINE_COMMENT_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_SINGLE_LINE_COMMENT_COLOR, new RGB(63, 127, 95)), fireEvent);
				RGB.iRGB(63, 127, 95),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_JAVA_KEYWORD_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_JAVA_KEYWORD_COLOR, new RGB(127, 0, 85)), fireEvent);
				RGB.iRGB(127, 0, 85),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_JAVA_KEYWORD_RETURN_COLOR, new RGB(127, 0, 85)), fireEvent);
				RGB.iRGB(127, 0, 85),fireEvent);


		setDefault(
				store,
				PreferenceConstants.EDITOR_JAVA_OPERATOR_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_JAVA_OPERATOR_COLOR, new RGB(0, 0, 0)), fireEvent);
				RGB.iRGB(0, 0, 0),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_JAVA_BRACKET_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_JAVA_BRACKET_COLOR, new RGB(0, 0, 0)), fireEvent);
				RGB.iRGB(0, 0, 0),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_MATCHING_BRACKETS_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_MATCHING_BRACKETS_COLOR, new RGB(192, 192,192)), fireEvent);
				RGB.iRGB(192, 192,192),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_TASK_TAG_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_TASK_TAG_COLOR, new RGB(127, 159, 191)), fireEvent);
				RGB.iRGB(127, 159, 191),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_JAVADOC_KEYWORD_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_JAVADOC_KEYWORD_COLOR, new RGB(127, 159, 191)), fireEvent);
				RGB.iRGB(127, 159, 191),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_JAVADOC_TAG_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_JAVADOC_TAG_COLOR, new RGB(127, 127, 159)), fireEvent);
				RGB.iRGB(127, 127, 159),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_JAVADOC_LINKS_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_JAVADOC_LINKS_COLOR, new RGB(63, 63, 191)), fireEvent);
				RGB.iRGB(63, 63, 191),fireEvent);

		setDefault(
				store,
				PreferenceConstants.EDITOR_JAVADOC_DEFAULT_COLOR,
//				findRGB(registry, IJavaThemeConstants.EDITOR_JAVADOC_DEFAULT_COLOR, new RGB(63, 95, 191)), fireEvent);
				RGB.iRGB(63, 95, 191),fireEvent);

	}
	
	/**
	 * Sets the default value and fires a property
	 * change event if necessary.
	 *
	 * @param store	the preference store
	 * @param key the preference key
	 * @param newValue the new value
	 * @param fireEvent <code>false</code> if no event should be fired
	 * @since 3.4
	 */
	private static void setDefault(IPreferenceStore store, String key, RGB newValue, boolean fireEvent) {
		if (!fireEvent) {
			PreferenceConverter.setDefault(store, key, newValue);
			return;
		}

		RGB oldValue= null;
		if (store.isDefault(key))
			oldValue= PreferenceConverter.getDefaultColor(store, key);

		PreferenceConverter.setDefault(store, key, newValue);

		if (oldValue != null && !oldValue.equals(newValue))
			store.firePropertyChangeEvent(key, oldValue, newValue);
	}
}
