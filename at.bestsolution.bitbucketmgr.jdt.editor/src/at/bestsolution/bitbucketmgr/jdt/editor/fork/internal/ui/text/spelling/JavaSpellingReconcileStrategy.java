package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.spelling;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.content.IContentType;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.source.IAnnotationModel;

import org.eclipse.jdt.core.IProblemRequestor;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.compiler.IProblem;

import at.bestsolution.efxclipse.text.editor.EditorsUI;
import at.bestsolution.efxclipse.text.editor.IDocumentProvider;
import at.bestsolution.efxclipse.text.editor.IEditorInput;
import at.bestsolution.efxclipse.text.editor.ITextEditor;
import at.bestsolution.efxclipse.text.editor.spelling.ISpellingProblemCollector;
import at.bestsolution.efxclipse.text.editor.spelling.SpellingProblem;
import at.bestsolution.efxclipse.text.editor.spelling.SpellingReconcileStrategy;
import at.bestsolution.efxclipse.text.editor.spelling.SpellingService;
import at.bestsolution.efxclipse.text.jface.source.ISourceViewer;


/**
 * Reconcile strategy for spell checking comments.
 *
 * @since 3.1
 */
public class JavaSpellingReconcileStrategy extends SpellingReconcileStrategy {


	/**
	 * Spelling problem collector that forwards {@link SpellingProblem}s as
	 * {@link IProblem}s to the {@link IProblemRequestor}.
	 */
	private class SpellingProblemCollector implements ISpellingProblemCollector {

		/*
		 * @see org.eclipse.ui.texteditor.spelling.ISpellingProblemCollector#accept(org.eclipse.ui.texteditor.spelling.SpellingProblem)
		 */
		public void accept(SpellingProblem problem) {
			IProblemRequestor requestor= fRequestor;
			if (requestor != null) {
				try {
					int line= getDocument().getLineOfOffset(problem.getOffset()) + 1;
					String word= getDocument().get(problem.getOffset(), problem.getLength());
					boolean dictionaryMatch= false;
					boolean sentenceStart= false;
					if (problem instanceof JavaSpellingProblem) {
						dictionaryMatch= ((JavaSpellingProblem)problem).isDictionaryMatch();
						sentenceStart= ((JavaSpellingProblem) problem).isSentenceStart();
					}
					// see https://bugs.eclipse.org/bugs/show_bug.cgi?id=81514
					IEditorInput editorInput= fEditor.getEditorInput();
					if (editorInput != null) {
						CoreSpellingProblem iProblem= new CoreSpellingProblem(problem.getOffset(), problem.getOffset() + problem.getLength() - 1, line, problem.getMessage(), word, dictionaryMatch, sentenceStart, getDocument(), editorInput.getName());
						requestor.acceptProblem(iProblem);
					}
				} catch (BadLocationException x) {
					// drop this SpellingProblem
				}
			}
		}

		/*
		 * @see org.eclipse.ui.texteditor.spelling.ISpellingProblemCollector#beginCollecting()
		 */
		public void beginCollecting() {
			if (fRequestor != null)
				fRequestor.beginReporting();
		}

		/*
		 * @see org.eclipse.ui.texteditor.spelling.ISpellingProblemCollector#endCollecting()
		 */
		public void endCollecting() {
			if (fRequestor != null)
				fRequestor.endReporting();
		}
	}


	/** The id of the problem */
	public static final int SPELLING_PROBLEM_ID= 0x80000000;

	/** Properties file content type */
	private static final IContentType JAVA_CONTENT_TYPE= Platform.getContentTypeManager().getContentType(JavaCore.JAVA_SOURCE_CONTENT_TYPE);

	/** The text editor to operate on. */
	private ITextEditor fEditor;

	/** The problem requester. */
	private IProblemRequestor fRequestor;


	/**
	 * Creates a new comment reconcile strategy.
	 *
	 * @param viewer the source viewer
	 * @param editor the text editor to operate on
	 */
	public JavaSpellingReconcileStrategy(ISourceViewer viewer, ITextEditor editor) {
		super(viewer, EditorsUI.getSpellingService());
		fEditor= editor;
	}

	/*
	 * @see org.eclipse.jface.text.reconciler.IReconcilingStrategy#reconcile(org.eclipse.jface.text.IRegion)
	 */
	@Override
	public void reconcile(IRegion region) {
		if (fRequestor != null && isSpellingEnabled())
			super.reconcile(region);
	}

	private boolean isSpellingEnabled() {
		return EditorsUI.getPreferenceStore().getBoolean(SpellingService.PREFERENCE_SPELLING_ENABLED);
	}

	/*
	 * @see org.eclipse.ui.texteditor.spelling.SpellingReconcileStrategy#createSpellingProblemCollector()
	 * @since 3.3
	 */
	@Override
	protected ISpellingProblemCollector createSpellingProblemCollector() {
		return new SpellingProblemCollector();
	}

	/*
	 * @see org.eclipse.ui.texteditor.spelling.SpellingReconcileStrategy#getContentType()
	 * @since 3.3
	 */
	@Override
	protected IContentType getContentType() {
		return JAVA_CONTENT_TYPE;
	}

	/*
	 * @see org.eclipse.jface.text.reconciler.IReconcilingStrategy#setDocument(org.eclipse.jface.text.IDocument)
	 */
	@Override
	public void setDocument(IDocument document) {
		super.setDocument(document);
		updateProblemRequester();
	}

	/**
	 * Update the problem requester based on the current editor
	 */
	private void updateProblemRequester() {
		IAnnotationModel model= getAnnotationModel();
		fRequestor= (model instanceof IProblemRequestor) ? (IProblemRequestor) model : null;
	}

	/*
	 * @see org.eclipse.ui.texteditor.spelling.SpellingReconcileStrategy#getAnnotationModel()
	 * @since 3.3
	 */
	@Override
	protected IAnnotationModel getAnnotationModel() {
		final IDocumentProvider documentProvider= fEditor.getDocumentProvider();
		if (documentProvider == null)
			return null;
		return documentProvider.getAnnotationModel(fEditor.getEditorInput());
	}
}
