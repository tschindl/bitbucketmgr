package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text;

import at.bestsolution.efxclipse.jface.RGB;

/**
 * Extends {@link org.eclipse.jdt.ui.text.IColorManager} with
 * the ability to bind and un-bind colors.
 *
 * @since 2.0
 */
public interface IColorManagerExtension {

	/**
	 * Remembers the given color specification under the given key.
	 *
	 * @param key the color key
	 * @param rgb the color specification
	 * @throws java.lang.UnsupportedOperationException if there is already a
	 * 	color specification remembered under the given key
	 */
	void bindColor(String key, RGB rgb);


	/**
	 * Forgets the color specification remembered under the given key.
	 *
	 * @param key the color key
	 */
	void unbindColor(String key);
}
