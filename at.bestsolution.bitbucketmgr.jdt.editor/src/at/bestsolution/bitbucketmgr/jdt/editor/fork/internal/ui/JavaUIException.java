package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui;


import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;

/**
 * An exception to wrap a status. This is necessary to use the core's IRunnableWithProgress
 * support
 */

public class JavaUIException extends CoreException {

	private static final long serialVersionUID= 1L;

	public JavaUIException(IStatus status) {
		super(status);
	}
}
