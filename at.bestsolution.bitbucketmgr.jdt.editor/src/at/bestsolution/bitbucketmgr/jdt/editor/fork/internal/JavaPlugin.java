package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.WorkingCopyOwner;
import org.osgi.framework.BundleContext;
import org.osgi.service.prefs.BackingStoreException;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.template.java.CodeTemplateContextType;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.util.TypeFilter;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.PreferenceConstants;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.ASTProvider;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.CompilationUnitDocumentProvider;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.DocumentAdapter;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.ICompilationUnitDocumentProvider;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.WorkingCopyManager;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.saveparticipants.SaveParticipantRegistry;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.preferences.MembersOrderPreferenceCache;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.JavaTextTools;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.PreferencesAdapter;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.ContentAssistHistory;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.viewsupport.ImageDescriptorRegistry;
import at.bestsolution.efxclipse.jface.preferences.IPreferenceStore;
import at.bestsolution.efxclipse.jface.preferences.ScopedPreferenceStore;
import at.bestsolution.efxclipse.text.editor.ChainedPreferenceStore;
import at.bestsolution.efxclipse.text.editor.EditorsUI;
import at.bestsolution.efxclipse.text.editor.text.templates.ContributionContextTypeRegistry;
import at.bestsolution.efxclipse.text.editor.text.templates.ContributionTemplateStore;
import at.bestsolution.efxclipse.text.jface.templates.ContextTypeRegistry;
import at.bestsolution.efxclipse.text.jface.templates.persistence.TemplateStore;

public class JavaPlugin extends Plugin {

	public static final String PLUGIN_ID = "at.bestsolution.bitbucketmgr.jdt.editor";

	/**
	 * The key to store customized code templates.
	 * @since 3.0
	 */
	private static final String CODE_TEMPLATES_KEY= "org.eclipse.jdt.ui.text.custom_code_templates"; //$NON-NLS-1$

	private static JavaPlugin INSTANCE;

	private JavaTextTools javaTextTools;

	private ScopedPreferenceStore preferenceStore;

	private SaveParticipantRegistry saveParticipantRegistry;

	private ContributionContextTypeRegistry codeTemplateContextTypeRegistry;

	private TypeFilter typeFilter;

	private WorkingCopyManager workingCopyManager;

	private CompilationUnitDocumentProvider compilationUnitDocumentProvider;

	private ASTProvider astProvider;

	private ContributionTemplateStore codeTemplateStore;

	private MembersOrderPreferenceCache membersOrderPreferenceCache;

	private ChainedPreferenceStore combinedPreferenceStore;

	private ContentAssistHistory fContentAssistHistory;

	private ImageDescriptorRegistry imageDescriptorRegistry;
	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		INSTANCE = this;
		
		WorkingCopyOwner.setPrimaryBufferProvider(new WorkingCopyOwner() {
			@Override
			public IBuffer createBuffer(ICompilationUnit workingCopy) {
				ICompilationUnit original= workingCopy.getPrimary();
				IResource resource= original.getResource();
				if (resource instanceof IFile)
					return new DocumentAdapter(workingCopy, (IFile) resource);
				return DocumentAdapter.NULL;
			}
		});
		
		IPreferenceStore store= getPreferenceStore();

		// must add here to guarantee that it is the first in the listener list
		membersOrderPreferenceCache= new MembersOrderPreferenceCache();
		membersOrderPreferenceCache.install(store);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		super.stop(context); 
	}

	public static void log(Throwable e) {
		// TODO Auto-generated method stub
		
	}

	public static String getPluginId() {
		return PLUGIN_ID;
	}

	public static JavaPlugin getDefault() {
		return INSTANCE;
	}
	
	public synchronized JavaTextTools getJavaTextTools() {
		if (javaTextTools == null)
			javaTextTools= new JavaTextTools(getPreferenceStore(), getJavaCorePluginPreferences());
		return javaTextTools;
	}
	
	public IPreferenceStore getPreferenceStore() {
        // Create the preference store lazily.
        if (preferenceStore == null) {
            preferenceStore = new ScopedPreferenceStore(new InstanceScope(),getBundle().getSymbolicName());

        }
        return preferenceStore;
    }
	
	/**
	 * Returns the Java Core plug-in preferences.
	 * 
	 * @return the Java Core plug-in preferences
	 * @since 3.7
	 */
	public static org.eclipse.core.runtime.Preferences getJavaCorePluginPreferences() {
		return JavaCore.getPlugin().getPluginPreferences();
	}
	
	public static void log(IStatus status) {
		// TODO Auto-generated method stub
		
	}
	
	public static IWorkspace getWorkspace() {
		return ResourcesPlugin.getWorkspace();
	}
	
	/**
	 * Returns the save participant registry.
	 *
	 * @return the save participant registry, not null
	 * @since 3.3
	 */
	public synchronized SaveParticipantRegistry getSaveParticipantRegistry() {
		if (saveParticipantRegistry == null)
			saveParticipantRegistry= new SaveParticipantRegistry();
		return saveParticipantRegistry;
	}

	/**
	 * Returns the template context type registry for the code generation
	 * templates.
	 *
	 * @return the template context type registry for the code generation
	 *         templates
	 * @since 3.0
	 */
	public ContextTypeRegistry getCodeTemplateContextRegistry() {
		if (codeTemplateContextTypeRegistry == null) {
			codeTemplateContextTypeRegistry= new ContributionContextTypeRegistry();

			CodeTemplateContextType.registerContextTypes(codeTemplateContextTypeRegistry);
		}

		return codeTemplateContextTypeRegistry;
	}
	
	public synchronized TypeFilter getTypeFilter() {
		if (typeFilter == null)
			typeFilter= new TypeFilter();
		return typeFilter;
	}
	
	public synchronized WorkingCopyManager getWorkingCopyManager() {
		if (workingCopyManager == null) {
			ICompilationUnitDocumentProvider provider= getCompilationUnitDocumentProvider();
			workingCopyManager= new WorkingCopyManager(provider);
		}
		return workingCopyManager;
	}

	public synchronized ICompilationUnitDocumentProvider getCompilationUnitDocumentProvider() {
		if (compilationUnitDocumentProvider == null)
			compilationUnitDocumentProvider= new CompilationUnitDocumentProvider();
		return compilationUnitDocumentProvider;
	}

	/**
	 * Returns the AST provider.
	 *
	 * @return the AST provider
	 * @since 3.0
	 */
	public synchronized ASTProvider getASTProvider() {
		if (astProvider == null)
			astProvider= new ASTProvider();

		return astProvider;
	}

	/**
	 * Returns the template store for the code generation templates.
	 *
	 * @return the template store for the code generation templates
	 * @since 3.0
	 */
	public TemplateStore getCodeTemplateStore() {
		if (codeTemplateStore == null) {
			IPreferenceStore store= getPreferenceStore();
//			boolean alreadyMigrated= store.getBoolean(CODE_TEMPLATES_MIGRATION_KEY);
//			if (alreadyMigrated)
				codeTemplateStore= new ContributionTemplateStore(getCodeTemplateContextRegistry(), store, CODE_TEMPLATES_KEY);
//			else {
//				codeTemplateStore= new CompatibilityTemplateStore(getCodeTemplateContextRegistry(), store, CODE_TEMPLATES_KEY, getOldCodeTemplateStoreInstance());
//				store.setValue(CODE_TEMPLATES_MIGRATION_KEY, true);
//			}

			try {
				codeTemplateStore.load();
			} catch (IOException e) {
				log(e);
			}

			codeTemplateStore.startListeningForPreferenceChanges();

			// compatibility / bug fixing code for duplicated templates
			// TODO remove for 3.0
//			CompatibilityTemplateStore.pruneDuplicates(codeTemplateStore, true);

		}

		return codeTemplateStore;
	}

	public synchronized MembersOrderPreferenceCache getMemberOrderPreferenceCache() {
		// initialized on startup
		return membersOrderPreferenceCache;
	}

	/**
	 * Returns a combined preference store, this store is read-only.
	 *
	 * @return the combined preference store
	 *
	 * @since 3.0
	 */
	public IPreferenceStore getCombinedPreferenceStore() {
		if (combinedPreferenceStore == null) {
			IPreferenceStore generalTextStore= EditorsUI.getPreferenceStore();
			combinedPreferenceStore= new ChainedPreferenceStore(new IPreferenceStore[] { getPreferenceStore(), new PreferencesAdapter(getJavaCorePluginPreferences()), generalTextStore });
		}
		return combinedPreferenceStore;
	}

	public static void logErrorMessage(String string) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Flushes the instance scope of this plug-in.
	 * 
	 * @since 3.7
	 */
	public static void flushInstanceScope() {
		try {
			InstanceScope.INSTANCE.getNode(PLUGIN_ID).flush();
		} catch (BackingStoreException e) {
			log(e);
		}
	}

	/**
	 * Returns the Java content assist history.
	 *
	 * @return the Java content assist history
	 * @since 3.2
	 */
	public ContentAssistHistory getContentAssistHistory() {
		if (fContentAssistHistory == null) {
			try {
				fContentAssistHistory= ContentAssistHistory.load(getPluginPreferences(), PreferenceConstants.CODEASSIST_LRU_HISTORY);
			} catch (CoreException x) {
				log(x); 
			}
			if (fContentAssistHistory == null)
				fContentAssistHistory= new ContentAssistHistory();
		}

		return fContentAssistHistory;
	}

	public static ImageDescriptorRegistry getImageDescriptorRegistry() {
		return getDefault().internalGetImageDescriptorRegistry();
	}
	
	private synchronized ImageDescriptorRegistry internalGetImageDescriptorRegistry() {
		if (imageDescriptorRegistry == null)
			imageDescriptorRegistry= new ImageDescriptorRegistry();
		return imageDescriptorRegistry;
	}

}
