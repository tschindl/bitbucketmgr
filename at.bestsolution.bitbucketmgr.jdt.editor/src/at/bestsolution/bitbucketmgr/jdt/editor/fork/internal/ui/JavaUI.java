package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui;

import java.net.URL;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaModelException;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.JavaPlugin;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.javadoc.JavaDocLocations;

public class JavaUI {
	/**
	 * The id of the Java plug-in (value <code>"org.eclipse.jdt.ui"</code>).
	 */
	public static final String ID_PLUGIN= JavaPlugin.PLUGIN_ID; //$NON-NLS-1$

	/**
	 * Returns the Javadoc base URL for an element. The base location contains the
	 * index file. This location doesn't have to exist. Returns <code>null</code>
	 * if no javadoc location has been attached to the element's library or project.
	 * Example of a returned URL is <i>http://www.junit.org/junit/javadoc</i>.
	 *
	 * @param element the element for which the documentation URL is requested.
	 * @return the base location
	 * @throws JavaModelException thrown when the element can not be accessed
	 *
	 * @since 2.0
	 */
	public static URL getJavadocBaseLocation(IJavaElement element) throws JavaModelException {
		return JavaDocLocations.getJavadocBaseLocation(element); 
	}
}
