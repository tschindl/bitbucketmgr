package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.refactoring.typeconstraints.types;


public class TypeTuple {
	private TType fFirst;
	private TType fSecond;

	public TypeTuple(TType first, TType second) {
		super();
		fFirst= first;
		fSecond= second;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof TypeTuple))
			return false;
		TypeTuple other= (TypeTuple)obj;
		return fFirst.equals(other.fFirst) && fSecond.equals(other.fSecond);
	}

	@Override
	public int hashCode() {
		return fFirst.hashCode() << 16 + fSecond.hashCode();
	}
}
