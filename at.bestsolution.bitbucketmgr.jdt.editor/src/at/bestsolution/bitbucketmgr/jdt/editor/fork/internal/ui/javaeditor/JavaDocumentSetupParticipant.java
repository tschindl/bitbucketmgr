package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;

import org.eclipse.jface.text.IDocument;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.JavaPlugin;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.IJavaPartitions;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.JavaTextTools;

/**
 * The document setup participant for JDT.
 */
public class JavaDocumentSetupParticipant  implements IDocumentSetupParticipant {

	public JavaDocumentSetupParticipant() {
	}

	/*
	 * @see org.eclipse.core.filebuffers.IDocumentSetupParticipant#setup(org.eclipse.jface.text.IDocument)
	 */
	public void setup(IDocument document) {
		JavaTextTools tools= JavaPlugin.getDefault().getJavaTextTools();
		tools.setupJavaDocumentPartitioner(document, IJavaPartitions.JAVA_PARTITIONING);
	}
}
