package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.javadoc;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.JavaContentAssistInvocationContext;
import at.bestsolution.efxclipse.text.editor.ITextEditor;
import at.bestsolution.efxclipse.text.jface.ITextViewer;


/**
 *
 * @since 3.2
 */
public final class JavadocContentAssistInvocationContext extends JavaContentAssistInvocationContext {

	private final int fFlags;

	/**
	 * @param viewer
	 * @param offset
	 * @param editor
	 * @param flags see {@link org.eclipse.jdt.ui.text.java.IJavadocCompletionProcessor#RESTRICT_TO_MATCHING_CASE}
	 */
	public JavadocContentAssistInvocationContext(ITextViewer viewer, int offset, ITextEditor editor, int flags) {
		super(viewer, offset, editor);
		fFlags= flags;
	}

	/**
	 * Returns the flags for this content assist invocation.
	 *
	 * @return the flags for this content assist invocation
	 * @see org.eclipse.jdt.ui.text.java.IJavadocCompletionProcessor#RESTRICT_TO_MATCHING_CASE
	 */
	public int getFlags() {
		return fFlags;
	}

	/**
	 * Returns the selection length of the viewer.
	 *
	 * @return the selection length of the viewer
	 */
	public int getSelectionLength() {
		return getViewer().getSelectedRange().length;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.TextContentAssistInvocationContext#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj))
			return false;

		return fFlags == ((JavadocContentAssistInvocationContext) obj).fFlags;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.TextContentAssistInvocationContext#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode() << 2 | fFlags;
	}

}
