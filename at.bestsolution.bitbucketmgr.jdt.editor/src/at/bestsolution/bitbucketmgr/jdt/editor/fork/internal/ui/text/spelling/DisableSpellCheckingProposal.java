package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.spelling;

import javafx.scene.image.Image;

import org.eclipse.jface.text.IDocument;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.JavaPluginImages;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.JavaUIMessages;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.IJavaCompletionProposal;
import at.bestsolution.efxclipse.jface.preferences.IPreferenceStore;
import at.bestsolution.efxclipse.styledtext.TextSelection;
import at.bestsolution.efxclipse.text.editor.EditorsUI;
import at.bestsolution.efxclipse.text.editor.spelling.SpellingService;
import at.bestsolution.efxclipse.text.jface.contentassist.IContextInformation;
import at.bestsolution.efxclipse.text.jface.quickassist.IQuickAssistInvocationContext;


/**
 * Proposal to disable spell checking.
 *
 * @since 3.3
 */
public class DisableSpellCheckingProposal implements IJavaCompletionProposal {

	/** The invocation context */
	private IQuickAssistInvocationContext fContext;

	/**
	 * Creates a new proposal.
	 *
	 * @param context the invocation context
	 */
	public DisableSpellCheckingProposal(IQuickAssistInvocationContext context) {
		fContext= context;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#apply(org.eclipse.jface.text.IDocument)
	 */
	public final void apply(final IDocument document) {
		IPreferenceStore store= EditorsUI.getPreferenceStore();
		store.setValue(SpellingService.PREFERENCE_SPELLING_ENABLED, false);
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getAdditionalProposalInfo()
	 */
	public String getAdditionalProposalInfo() {
		return JavaUIMessages.Spelling_disable_info;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getContextInformation()
	 */
	public final IContextInformation getContextInformation() {
		return null;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getDisplayString()
	 */
	public String getDisplayString() {
		return JavaUIMessages.Spelling_disable_label;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getImage()
	 */
	public Image getImage() {
		return JavaPluginImages.get(JavaPluginImages.IMG_OBJS_NLS_NEVER_TRANSLATE);
	}
	/*
	 * @see org.eclipse.jdt.ui.text.java.IJavaCompletionProposal#getRelevance()
	 */
	public final int getRelevance() {
		return Integer.MIN_VALUE + 1;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getSelection(org.eclipse.jface.text.IDocument)
	 */
	public final TextSelection getSelection(final IDocument document) {
		return new TextSelection(fContext.getOffset(), fContext.getLength());
	}
}
