package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;

import at.bestsolution.efxclipse.text.jface.TextPresentation;
import at.bestsolution.efxclipse.text.jface.presentation.PresentationReconciler;

public class JavaPresentationReconciler extends PresentationReconciler {
	/** Last used document */
	private IDocument fLastDocument;

	/**
	 * Constructs a "repair description" for the given damage and returns
	 * this description as a text presentation.
	 * <p>
	 * NOTE: Should not be used if this reconciler is installed on a viewer.
	 * </p>
	 *
	 * @param damage the damage to be repaired
	 * @param document the document whose presentation must be repaired
	 * @return the presentation repair description as text presentation
	 */
	public TextPresentation createRepairDescription(IRegion damage, IDocument document) {
		if (document != fLastDocument) {
			setDocumentToDamagers(document);
			setDocumentToRepairers(document);
			fLastDocument= document;
		}
		return createPresentation(damage, document);
	}
}
