package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor;

import org.eclipse.jdt.core.ICompilationUnit;


public interface ISavePolicy {

	/**
	 * Notifies this save policy that the given compilation unit
	 * is about to be saved.
	 *
	 * @param unit the compilation unit
	 */
	void preSave(ICompilationUnit unit);

	/**
	 * Returns the compilation unit in which the argument
	 * has been changed. If the argument is not changed, the
	 * returned result is <code>null</code>.
	 *
	 * @param unit the compilation unit
	 * @return the changed compilation unit or <code>null</code>
	 */
	ICompilationUnit postSave(ICompilationUnit unit);
}
