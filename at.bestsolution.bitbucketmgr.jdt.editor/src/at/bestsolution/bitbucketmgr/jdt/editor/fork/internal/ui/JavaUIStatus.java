package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.JavaPlugin;

/**
 * Convenience class for error exceptions thrown inside JavaUI plugin.
 */
public class JavaUIStatus extends Status {

	private JavaUIStatus(int severity, int code, String message, Throwable throwable) {
		super(severity, JavaPlugin.getPluginId(), code, message, throwable);
	}

	public static IStatus createError(int code, Throwable throwable) {
		String message= throwable.getMessage();
		if (message == null) {
			message= throwable.getClass().getName();
		}
		return new JavaUIStatus(IStatus.ERROR, code, message, throwable);
	}

	public static IStatus createError(int code, String message, Throwable throwable) {
		return new JavaUIStatus(IStatus.ERROR, code, message, throwable);
	}

	public static IStatus createWarning(int code, String message, Throwable throwable) {
		return new JavaUIStatus(IStatus.WARNING, code, message, throwable);
	}

	public static IStatus createInfo(int code, String message, Throwable throwable) {
		return new JavaUIStatus(IStatus.INFO, code, message, throwable);
	}
}

