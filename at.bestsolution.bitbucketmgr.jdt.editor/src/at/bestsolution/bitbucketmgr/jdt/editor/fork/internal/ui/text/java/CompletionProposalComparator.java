package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java;

import java.util.Comparator;

import at.bestsolution.efxclipse.text.jface.contentassist.ICompletionProposal;
import at.bestsolution.efxclipse.text.jface.templates.TemplateProposal;


/**
 * Comparator for java completion proposals. Completion proposals can be sorted by relevance or
 * alphabetically.
 * <p>
 * Note: this comparator imposes orderings that are inconsistent with equals.
 * </p>
 *
 * @since 3.1
 */
public final class CompletionProposalComparator implements Comparator<ICompletionProposal> {

	private boolean fOrderAlphabetically;

	/**
	 * Creates a comparator that sorts by relevance.
	 */
	public CompletionProposalComparator() {
		fOrderAlphabetically= false;
	}

	/**
	 * Sets the sort order. Default is <code>false</code>, i.e. order by
	 * relevance.
	 *
	 * @param orderAlphabetically <code>true</code> to order alphabetically,
	 *        <code>false</code> to order by relevance
	 */
	public void setOrderAlphabetically(boolean orderAlphabetically) {
		fOrderAlphabetically= orderAlphabetically;
	}

	/**
	 * {@inheritDoc}
	 * @since 3.7
	 */
	public int compare(ICompletionProposal p1, ICompletionProposal p2) {
		if (!fOrderAlphabetically) {
			int r1= getRelevance(p1);
			int r2= getRelevance(p2);
			int relevanceDif= r2 - r1;
			if (relevanceDif != 0) {
				return relevanceDif;
			}
		}
		/*
		 * TODO the correct (but possibly much slower) sorting would use a
		 * collator.
		 */
		// fix for bug 67468
		return getSortKey(p1).compareToIgnoreCase(getSortKey(p2));
	}

	private String getSortKey(ICompletionProposal p) {
		if (p instanceof AbstractJavaCompletionProposal)
			return ((AbstractJavaCompletionProposal) p).getSortString();
		return p.getDisplayString();
	}

	private int getRelevance(ICompletionProposal obj) {
		if (obj instanceof IJavaCompletionProposal) {
			IJavaCompletionProposal jcp= (IJavaCompletionProposal) obj;
			return jcp.getRelevance();
		} else if (obj instanceof TemplateProposal) {
			TemplateProposal tp= (TemplateProposal) obj;
			return tp.getRelevance();
		}
		// catch all
		return 0;
	}

}
