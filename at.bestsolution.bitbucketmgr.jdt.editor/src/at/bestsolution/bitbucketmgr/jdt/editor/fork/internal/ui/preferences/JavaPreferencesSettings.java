package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.preferences;

import org.eclipse.jdt.core.IJavaProject;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.codemanipulation.CodeGenerationSettings;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.util.CodeFormatterUtil;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.PreferenceConstants;


public class JavaPreferencesSettings  {

	public static CodeGenerationSettings getCodeGenerationSettings(IJavaProject project) {
		CodeGenerationSettings res= new CodeGenerationSettings();
		res.createComments= Boolean.valueOf(PreferenceConstants.getPreference(PreferenceConstants.CODEGEN_ADD_COMMENTS, project)).booleanValue();
		res.useKeywordThis= Boolean.valueOf(PreferenceConstants.getPreference(PreferenceConstants.CODEGEN_KEYWORD_THIS, project)).booleanValue();
		res.overrideAnnotation= Boolean.valueOf(PreferenceConstants.getPreference(PreferenceConstants.CODEGEN_USE_OVERRIDE_ANNOTATION, project)).booleanValue();
		res.importIgnoreLowercase= Boolean.valueOf(PreferenceConstants.getPreference(PreferenceConstants.ORGIMPORTS_IGNORELOWERCASE, project)).booleanValue();
		res.tabWidth= CodeFormatterUtil.getTabWidth(project);
		res.indentWidth= CodeFormatterUtil.getIndentWidth(project);
		return res;
	}

}

