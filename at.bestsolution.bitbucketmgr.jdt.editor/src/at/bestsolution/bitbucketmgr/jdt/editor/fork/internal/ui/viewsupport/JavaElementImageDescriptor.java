package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.viewsupport;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import at.bestsolution.efxclipse.jface.resources.ImageDescriptor;

public class JavaElementImageDescriptor extends ImageDescriptor {

	/** Flag to render the abstract adornment. */
	public final static int ABSTRACT= 		0x001;

	/** Flag to render the final adornment. */
	public final static int FINAL=			0x002;

	/** Flag to render the synchronized adornment. */
	public final static int SYNCHRONIZED=	0x004;

	/** Flag to render the static adornment. */
	public final static int STATIC=			0x008;

	/** Flag to render the runnable adornment. */
	public final static int RUNNABLE= 		0x010;

	/** Flag to render the warning adornment. */
	public final static int WARNING=			0x020;

	/** Flag to render the error adornment. */
	public final static int ERROR=			0x040;

	/** Flag to render the 'override' adornment. */
	public final static int OVERRIDES= 		0x080;

	/** Flag to render the 'implements' adornment. */
	public final static int IMPLEMENTS= 		0x100;

	/** Flag to render the 'constructor' adornment. */
	public final static int CONSTRUCTOR= 	0x200;

	/**
	 * Flag to render the 'deprecated' adornment.
	 * @since 3.0
	 */
	public final static int DEPRECATED= 	0x400;

	/**
	 * Flag to render the 'volatile' adornment.
	 * @since 3.3
	 */
	public final static int VOLATILE= 	0x800;

	/**
	 * Flag to render the 'transient' adornment.
	 * @since 3.3
	 */
	public final static int TRANSIENT= 	0x1000;

	/**
	 * Flag to render the build path error adornment.
	 * @since 3.7
	 */
	public final static int BUILDPATH_ERROR= 0x2000;

	/**
	 * Flag to render the 'native' adornment.
	 * @since 3.7
	 */
	public final static int NATIVE= 	0x4000;

	/**
	 * Flag to render the 'ignore optional compile problems' adornment.
	 * @since 3.8
	 */
	public final static int IGNORE_OPTIONAL_PROBLEMS= 0x8000;

	private ImageDescriptor fBaseImage;
	private int fFlags;
	private Point2D fSize;
	
	public JavaElementImageDescriptor(ImageDescriptor baseImage, int flags, Point2D size) {
		this.fBaseImage = baseImage;
		this.fFlags = flags;
		this.fSize = size;
	}

	@Override
	public Image createImage() {
		return fBaseImage.createImage();
	}
}
