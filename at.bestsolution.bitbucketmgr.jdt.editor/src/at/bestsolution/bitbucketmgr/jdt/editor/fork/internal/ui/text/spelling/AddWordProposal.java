package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.spelling;

import javafx.scene.image.Image;

import org.eclipse.jface.text.IDocument;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.util.Messages;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.JavaPluginImages;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.JavaUIMessages;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.PreferenceConstants;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.IJavaCompletionProposal;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.spelling.engine.ISpellCheckEngine;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.spelling.engine.ISpellChecker;
import at.bestsolution.efxclipse.styledtext.TextSelection;
import at.bestsolution.efxclipse.text.editor.spelling.SpellingProblem;
import at.bestsolution.efxclipse.text.jface.contentassist.IContextInformation;
import at.bestsolution.efxclipse.text.jface.quickassist.IQuickAssistInvocationContext;

/**
 * Proposal to add the unknown word to the dictionaries.
 *
 * @since 3.0
 */
public class AddWordProposal implements IJavaCompletionProposal {

	private static final String PREF_KEY_DO_NOT_ASK= "do_not_ask_to_install_user_dictionary"; //$NON-NLS-1$

	/** The invocation context */
	private final IQuickAssistInvocationContext fContext;

	/** The word to add */
	private final String fWord;


	/**
	 * Creates a new add word proposal
	 *
	 * @param word
	 *                   The word to add
	 * @param context
	 *                   The invocation context
	 */
	public AddWordProposal(final String word, final IQuickAssistInvocationContext context) {
		fContext= context;
		fWord= word;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#apply(org.eclipse.jface.text.IDocument)
	 */
	public final void apply(final IDocument document) {

		final ISpellCheckEngine engine= SpellCheckEngine.getInstance();
		final ISpellChecker checker= engine.getSpellChecker();

		if (checker == null)
			return;

//		if (!checker.acceptsWords()) {
//			final Shell shell;
//			if (fContext != null && fContext.getSourceViewer() != null)
//				shell= fContext.getSourceViewer().getTextWidget().getShell();
//			else
//				shell= JavaPlugin.getActiveWorkbenchShell();
//
//			if (!canAskToConfigure() || !askUserToConfigureUserDictionary(shell))
//				return;
//
//			String[] preferencePageIds= new String[] { "org.eclipse.ui.editors.preferencePages.Spelling" }; //$NON-NLS-1$
//			PreferencesUtil.createPreferenceDialogOn(shell, preferencePageIds[0], preferencePageIds, null).open();
//		}

		if (checker.acceptsWords()) {
			checker.addWord(fWord);
			if (fContext != null && fContext.getSourceViewer() != null)
				SpellingProblem.removeAll(fContext.getSourceViewer(), fWord);
		}
	}

//	/**
//	 * Asks the user whether he wants to configure a user dictionary.
//	 * 
//	 * @param shell the shell
//	 * @return <code>true</code> if the user wants to configure the user dictionary
//	 * @since 3.3
//	 */
//	private boolean askUserToConfigureUserDictionary(Shell shell) {
//		MessageDialogWithToggle toggleDialog= MessageDialogWithToggle.openYesNoQuestion(
//				shell,
//				JavaUIMessages.Spelling_add_askToConfigure_title,
//				JavaUIMessages.Spelling_add_askToConfigure_question,
//				JavaUIMessages.Spelling_add_askToConfigure_ignoreMessage,
//				false,
//				null,
//				null);
//
//		PreferenceConstants.getPreferenceStore().setValue(PREF_KEY_DO_NOT_ASK, toggleDialog.getToggleState());
//
//		return toggleDialog.getReturnCode() == IDialogConstants.YES_ID;
//	}

	/**
	 * Tells whether this proposal can ask to 
	 * configure a user dictionary.
	 *
	 * @return <code>true</code> if it can ask the user
	 */
	static boolean canAskToConfigure() {
		return !PreferenceConstants.getPreferenceStore().getBoolean(PREF_KEY_DO_NOT_ASK);
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getAdditionalProposalInfo()
	 */
	public String getAdditionalProposalInfo() {
		return Messages.format(JavaUIMessages.Spelling_add_info, new String[] { WordCorrectionProposal.getHtmlRepresentation(fWord)});
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getContextInformation()
	 */
	public final IContextInformation getContextInformation() {
		return null;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getDisplayString()
	 */
	public String getDisplayString() {
		return Messages.format(JavaUIMessages.Spelling_add_label, new String[] { fWord });
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getImage()
	 */
	public Image getImage() {
		return JavaPluginImages.get(JavaPluginImages.IMG_CORRECTION_ADD);
	}

	/*
	 * @see org.eclipse.jdt.ui.text.java.IJavaCompletionProposal#getRelevance()
	 */
	public int getRelevance() {
		return Integer.MIN_VALUE;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getSelection(org.eclipse.jface.text.IDocument)
	 */
	public final TextSelection getSelection(final IDocument document) {
		return new TextSelection(fContext.getOffset(), fContext.getLength());
	}
}
