package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.viewsupport;

import java.util.HashMap;
import java.util.Iterator;

import javafx.scene.image.Image;

import org.eclipse.core.runtime.Assert;

import at.bestsolution.efxclipse.jface.resources.ImageDescriptor;

/**
 * A registry that maps <code>ImageDescriptors</code> to <code>Image</code>.
 */
public class ImageDescriptorRegistry {

	private HashMap<ImageDescriptor, Image> fRegistry= new HashMap<ImageDescriptor, Image>(10);

	/**
	 * Creates a new image descriptor registry for the given display. All images
	 * managed by this registry will be disposed when the display gets disposed.
	 */
	public ImageDescriptorRegistry() {
	}

	/**
	 * Returns the image associated with the given image descriptor.
	 *
	 * @param descriptor the image descriptor for which the registry manages an image,
	 *  or <code>null</code> for a missing image descriptor
	 * @return the image associated with the image descriptor or <code>null</code>
	 *  if the image descriptor can't create the requested image.
	 */
	public Image get(ImageDescriptor descriptor) {
		if (descriptor == null)
			descriptor= ImageDescriptor.getMissingImageDescriptor();

		Image result= fRegistry.get(descriptor);
		if (result != null)
			return result;

		result= descriptor.createImage();
		if (result != null)
			fRegistry.put(descriptor, result);
		return result;
	}

	/**
	 * Disposes all images managed by this registry.
	 */
	public void dispose() {
		for (Iterator<Image> iter= fRegistry.values().iterator(); iter.hasNext(); ) {
			Image image= iter.next();
//			image.dispose();
		}
		fRegistry.clear();
	}
}
