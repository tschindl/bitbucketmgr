package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.spelling;

import javafx.scene.image.Image;

import org.eclipse.jface.text.IDocument;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.util.Messages;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.JavaPluginImages;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.JavaUIMessages;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.IJavaCompletionProposal;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.spelling.engine.ISpellCheckEngine;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.spelling.engine.ISpellChecker;
import at.bestsolution.efxclipse.styledtext.TextSelection;
import at.bestsolution.efxclipse.text.editor.spelling.SpellingProblem;
import at.bestsolution.efxclipse.text.jface.contentassist.IContextInformation;
import at.bestsolution.efxclipse.text.jface.quickassist.IQuickAssistInvocationContext;
import at.bestsolution.efxclipse.text.jface.source.ISourceViewer;

/**
 * Proposal to ignore the word during the current editing session.
 *
 * @since 3.0
 */
public class WordIgnoreProposal implements IJavaCompletionProposal {

	/** The invocation context */
	private IQuickAssistInvocationContext fContext;

	/** The word to ignore */
	private String fWord;

	/**
	 * Creates a new spell ignore proposal.
	 *
	 * @param word
	 *                   The word to ignore
	 * @param context
	 *                   The invocation context
	 */
	public WordIgnoreProposal(final String word, final IQuickAssistInvocationContext context) {
		fWord= word;
		fContext= context;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#apply(org.eclipse.jface.text.IDocument)
	 */
	public final void apply(final IDocument document) {

		final ISpellCheckEngine engine= SpellCheckEngine.getInstance();
		final ISpellChecker checker= engine.getSpellChecker();

		if (checker != null) {
			checker.ignoreWord(fWord);
			ISourceViewer sourceViewer= fContext.getSourceViewer();
			if (sourceViewer != null)
				SpellingProblem.removeAll(sourceViewer, fWord);
		}
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getAdditionalProposalInfo()
	 */
	public String getAdditionalProposalInfo() {
		return Messages.format(JavaUIMessages.Spelling_ignore_info, new String[] { WordCorrectionProposal.getHtmlRepresentation(fWord)});
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getContextInformation()
	 */
	public final IContextInformation getContextInformation() {
		return null;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getDisplayString()
	 */
	public String getDisplayString() {
		return Messages.format(JavaUIMessages.Spelling_ignore_label, new String[] { fWord }); 
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getImage()
	 */
	public Image getImage() {
		return JavaPluginImages.get(JavaPluginImages.IMG_OBJS_NLS_NEVER_TRANSLATE);
	}
	/*
	 * @see org.eclipse.jdt.ui.text.java.IJavaCompletionProposal#getRelevance()
	 */
	public final int getRelevance() {
		return Integer.MIN_VALUE + 1;
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getSelection(org.eclipse.jface.text.IDocument)
	 */
	public final TextSelection getSelection(final IDocument document) {
		return new TextSelection(fContext.getOffset(), fContext.getLength());
	}
}
