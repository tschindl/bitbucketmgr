package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.spelling.engine;

/**
 * Interface of hashers to compute the phonetic hash for a word.
 *
 * @since 3.0
 */
public interface IPhoneticHashProvider {

	/**
	 * Returns the phonetic hash for the word.
	 *
	 * @param word
	 *                  The word to get the phonetic hash for
	 * @return The phonetic hash for the word
	 */
	public String getHash(String word);

	/**
	 * Returns an array of characters to compute possible mutations.
	 *
	 * @return Array of possible mutator characters
	 */
	public char[] getMutators();
}
