package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.javadoc;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.IJavaPartitions;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.ContentAssistInvocationContext;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.IJavadocCompletionProcessor;
import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java.JavaCompletionProcessor;
import at.bestsolution.efxclipse.text.editor.ITextEditor;
import at.bestsolution.efxclipse.text.jface.ITextViewer;
import at.bestsolution.efxclipse.text.jface.contentassist.ContentAssistant;
import at.bestsolution.efxclipse.text.jface.contentassist.IContextInformationValidator;


/**
 * Javadoc completion processor.
 *
 * @since 3.2
 */
public class JavadocCompletionProcessor extends JavaCompletionProcessor {

	private int fSubProcessorFlags;

	public JavadocCompletionProcessor(ITextEditor editor, ContentAssistant assistant) {
		super(editor, assistant, IJavaPartitions.JAVA_DOC);
		fSubProcessorFlags= 0;
	}

	/**
	 * Tells this processor to restrict is proposals to those
	 * starting with matching cases.
	 *
	 * @param restrict <code>true</code> if proposals should be restricted
	 */
	@Override
	public void restrictProposalsToMatchingCases(boolean restrict) {
		fSubProcessorFlags= restrict ? IJavadocCompletionProcessor.RESTRICT_TO_MATCHING_CASE : 0;
	}

	/**
	 * @see IContentAssistProcessor#getContextInformationValidator()
	 */
	@Override
	public IContextInformationValidator getContextInformationValidator() {
		return null;
	}

	/*
	 * @see org.eclipse.jdt.internal.ui.text.java.JavaCompletionProcessor#createContext(org.eclipse.jface.text.ITextViewer, int)
	 */
	@Override
	protected ContentAssistInvocationContext createContext(ITextViewer viewer, int offset) {
		return new JavadocContentAssistInvocationContext(viewer, offset, fEditor, fSubProcessorFlags);
	}

}
