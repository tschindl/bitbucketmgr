package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java;

import java.util.Hashtable;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

import at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor.EditorUtility;
import at.bestsolution.efxclipse.text.editor.ITextEditor;
import at.bestsolution.efxclipse.text.jface.ITextViewer;
import at.bestsolution.efxclipse.text.jface.contentassist.ContentAssistant;
import at.bestsolution.efxclipse.text.jface.contentassist.ICompletionProposal;
import at.bestsolution.efxclipse.text.jface.contentassist.IContextInformationValidator;


/**
 * Java completion processor.
 */
public class JavaCompletionProcessor extends ContentAssistProcessor {

	private final static String VISIBILITY= JavaCore.CODEASSIST_VISIBILITY_CHECK;
	private final static String ENABLED= "enabled"; //$NON-NLS-1$
	private final static String DISABLED= "disabled"; //$NON-NLS-1$

	private IContextInformationValidator fValidator;
	protected final ITextEditor fEditor;

	public JavaCompletionProcessor(ITextEditor editor, ContentAssistant assistant, String partition) {
		super(assistant, partition);
		fEditor= editor;
	}

	/**
	 * Tells this processor to restrict its proposal to those element
	 * visible in the actual invocation context.
	 *
	 * @param restrict <code>true</code> if proposals should be restricted
	 */
	public void restrictProposalsToVisibility(boolean restrict) {
		Hashtable<String, String> options= JavaCore.getOptions();
		Object value= options.get(VISIBILITY);
		if (value instanceof String) {
			String newValue= restrict ? ENABLED : DISABLED;
			if ( !newValue.equals(value)) {
				options.put(VISIBILITY, newValue);
				JavaCore.setOptions(options);
			}
		}
	}

	/**
	 * Tells this processor to restrict is proposals to those
	 * starting with matching cases.
	 *
	 * @param restrict <code>true</code> if proposals should be restricted
	 */
	public void restrictProposalsToMatchingCases(boolean restrict) {
		// not yet supported
	}

	/*
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getContextInformationValidator()
	 */
	@Override
	public IContextInformationValidator getContextInformationValidator() {
		if (fValidator == null)
			fValidator= new JavaParameterListValidator();
		return fValidator;
	}

	/*
	 * @see ContentAssistProcessor#checkDefaultEnablement(CompletionProposalCategory)
	 * @since 3.8
	 */
	@Override
	protected boolean checkDefaultEnablement(CompletionProposalCategory category) {
		return super.checkDefaultEnablement(category) && category.matches(getJavaProject());
	}

	/*
	 * @see ContentAssistProcessor#checkSeparateEnablement(CompletionProposalCategory)
	 * @since 3.8
	 */
	@Override
	protected boolean checkSeparateEnablement(CompletionProposalCategory category) {
		return super.checkSeparateEnablement(category) && category.matches(getJavaProject());
	}

	private IJavaProject getJavaProject() {
		return EditorUtility.getJavaProject(fEditor.getEditorInput());
	}
	
	/*
	 * @see org.eclipse.jdt.internal.ui.text.java.ContentAssistProcessor#filterAndSort(java.util.List, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected List<ICompletionProposal> sortProposals(List<ICompletionProposal> proposals, IProgressMonitor monitor, ContentAssistInvocationContext context) {
		ProposalSorterHandle h = ProposalSorterRegistry.getDefault().getCurrentSorter();
		if( h != null ) {
			h.sortProposals(context, proposals);	
		}
		return proposals;
	}

	/*
	 * @see org.eclipse.jdt.internal.ui.text.java.ContentAssistProcessor#createContext(org.eclipse.jface.text.ITextViewer, int)
	 */
	@Override
	protected ContentAssistInvocationContext createContext(ITextViewer viewer, int offset) {
		return new JavaContentAssistInvocationContext(viewer, offset, fEditor);
	}
}
