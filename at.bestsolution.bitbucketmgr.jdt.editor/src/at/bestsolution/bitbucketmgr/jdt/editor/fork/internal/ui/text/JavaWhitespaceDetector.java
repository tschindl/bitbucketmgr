package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text;

import at.bestsolution.efxclipse.text.jface.rules.IWhitespaceDetector;

/**
 * A java aware white space detector.
 */
public class JavaWhitespaceDetector implements IWhitespaceDetector {

	public boolean isWhitespace(char c) {
		return Character.isWhitespace(c);
	}

}
