package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor;

import org.eclipse.jdt.core.IClassFile;

import at.bestsolution.efxclipse.text.editor.IEditorInput;


/**
 * Editor input for class files.
 */
public interface IClassFileEditorInput extends IEditorInput {

	/**
	 * Returns the class file acting as input.
	 * 
	 * @return the class file
	 */
	public IClassFile getClassFile();
}
