package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javafx.scene.paint.Color;

import at.bestsolution.efxclipse.jface.RGB;

/**
 * Java color manager.
 */
public class JavaColorManager implements IColorManager, IColorManagerExtension {

	protected Map<String, RGB> fKeyTable= new HashMap<String, RGB>(10);
	protected Map<RGB, Color> fDisplayTable= new HashMap<RGB, Color>(2);

	/**
	 * Flag which tells if the colors are automatically disposed when
	 * the current display gets disposed.
	 */
	private boolean fAutoDisposeOnDisplayDispose;


	/**
	 * Creates a new Java color manager which automatically
	 * disposes the allocated colors when the current display
	 * gets disposed.
	 */
	public JavaColorManager() {
		this(true);
	}

	/**
	 * Creates a new Java color manager.
	 *
	 * @param autoDisposeOnDisplayDispose 	if <code>true</code>  the color manager
	 * automatically disposes all managed colors when the current display gets disposed
	 * and all calls to {@link org.eclipse.jface.text.source.ISharedTextColors#dispose()} are ignored.
	 *
	 * @since 2.1
	 */
	public JavaColorManager(boolean autoDisposeOnDisplayDispose) {
		fAutoDisposeOnDisplayDispose= autoDisposeOnDisplayDispose;
	}

	/*
	 * @see IColorManager#getColor(RGB)
	 */
	public Color getColor(RGB rgb) {

		if (rgb == null)
			return null;

		Color color= fDisplayTable.get(rgb);
		if (color == null) {
			color= new Color(rgb.red,rgb.green,rgb.blue,rgb.opacity);
			fDisplayTable.put(rgb, color);
		}

		return color;
	}

	/*
	 * @see IColorManager#dispose
	 */
	public void dispose() {
		if (!fAutoDisposeOnDisplayDispose)
			fDisplayTable.clear();
	}

	/*
	 * @see IColorManager#getColor(String)
	 */
	public Color getColor(String key) {

		if (key == null)
			return null;

		RGB rgb= fKeyTable.get(key);
		return getColor(rgb);
	}

	/*
	 * @see IColorManagerExtension#bindColor(String, RGB)
	 */
	public void bindColor(String key, RGB rgb) {
		Object value= fKeyTable.get(key);
		if (value != null)
			throw new UnsupportedOperationException();

		fKeyTable.put(key, rgb);
	}

	/*
	 * @see IColorManagerExtension#unbindColor(String)
	 */
	public void unbindColor(String key) {
		fKeyTable.remove(key);
	}
}
