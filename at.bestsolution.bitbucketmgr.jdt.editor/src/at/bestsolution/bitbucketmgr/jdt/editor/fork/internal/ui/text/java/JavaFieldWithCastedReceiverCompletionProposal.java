package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java;

import javafx.scene.image.Image;

import org.eclipse.core.runtime.Assert;

import org.eclipse.jdt.core.CompletionProposal;

import at.bestsolution.efxclipse.jface.StyledString;


/**
 * Java completion proposal for {@link CompletionProposal#FIELD_REF_WITH_CASTED_RECEIVER}.
 *
 * @since 3.4
 */
public class JavaFieldWithCastedReceiverCompletionProposal extends JavaCompletionProposal {

	private CompletionProposal fProposal;

	public JavaFieldWithCastedReceiverCompletionProposal(String completion, int start, int length, Image image, StyledString label, int relevance, boolean inJavadoc, JavaContentAssistInvocationContext invocationContext, CompletionProposal proposal) {
		super(completion, start, length, image, label, relevance, inJavadoc, invocationContext);
		Assert.isNotNull(proposal);
		fProposal= proposal;
	}

	/*
	 * @see org.eclipse.jdt.internal.ui.text.java.AbstractJavaCompletionProposal#isPrefix(java.lang.String, java.lang.String)
	 */
	@Override
	protected boolean isPrefix(String prefix, String string) {
		if (prefix != null)
			prefix= prefix.substring(fProposal.getReceiverEnd() - fProposal.getReceiverStart() + 1);
		return super.isPrefix(prefix, string);
	}

}
