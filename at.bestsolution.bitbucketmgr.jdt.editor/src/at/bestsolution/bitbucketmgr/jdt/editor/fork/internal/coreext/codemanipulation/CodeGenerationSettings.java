package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.codemanipulation;

public class CodeGenerationSettings {

	public boolean createComments= true;
	public boolean useKeywordThis= false;

	public boolean importIgnoreLowercase= true;
	public boolean overrideAnnotation= false;

	public int tabWidth;
	public int indentWidth;



	public void setSettings(CodeGenerationSettings settings) {
		settings.createComments= createComments;
		settings.useKeywordThis= useKeywordThis;
		settings.importIgnoreLowercase= importIgnoreLowercase;
		settings.overrideAnnotation= overrideAnnotation;
		settings.tabWidth= tabWidth;
		settings.indentWidth= indentWidth;
	}


}

