package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java;

import java.util.Comparator;

import org.eclipse.core.runtime.IConfigurationElement;

import at.bestsolution.efxclipse.text.jface.contentassist.ICompletionProposal;
import at.bestsolution.efxclipse.text.jface.contentassist.ICompletionProposalSorter;

/**
 * Abstract base class for sorters contributed to the
 * <code>org.eclipse.jdt.ui.javaCompletionProposalSorters</code> extension point.
 * <p>
 * Subclasses need to implement {@link #compare(ICompletionProposal, ICompletionProposal)} and may
 * override {@link #beginSorting(ContentAssistInvocationContext) beginSorting} and
 * {@link #endSorting() endSorting}.
 * </p>
 * <p>
 * The orderings imposed by a subclass need not be consistent with equals.
 * </p>
 *
 * @since 3.2
 */
public abstract class AbstractProposalSorter implements Comparator<ICompletionProposal>, ICompletionProposalSorter {

	/**
	 * Creates a new sorter. Note that subclasses must provide a zero-argument constructor to be
	 * instantiatable via {@link IConfigurationElement#createExecutableExtension(String)}.
	 */
	protected AbstractProposalSorter() {
	}

	/**
	 * Called once before initial sorting starts the first time.
	 * <p>
	 * <strong>Note:</strong> As of 3.8 a completion proposal computer can request that proposals
	 * are resorted. If such a computer is active, then this method will not be called.
	 * </p>
	 * <p>
	 * Clients may override, the default implementation does nothing.
	 * </p>
	 * 
	 * @param context the context of the content assist invocation
	 */
	public void beginSorting(ContentAssistInvocationContext context) {
	}

	/**
	 * The orderings imposed by an implementation need not be consistent with equals.
	 * 
	 * @param p1 the first proposal to be compared
	 * @param p2 the second proposal to be compared
	 * @return a negative integer, zero, or a positive integer as the first argument is less than,
	 *         equal to, or greater than the second
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public abstract int compare(ICompletionProposal p1, ICompletionProposal p2);

	/**
	 * Called once after the initial sorting finished.
	 * <p>
	 * <strong>Note:</strong> As of 3.8 a completion proposal computer can request that proposals
	 * are resorted. If such a computer is active, then this method will not be called.
	 * </p>
	 * <p>
	 * Clients may override, the default implementation does nothing.
	 * </p>
	 */
	public void endSorting() {
	}
}
