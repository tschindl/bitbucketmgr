package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.dom;

import org.eclipse.jdt.core.dom.ITypeBinding;

/**
 * The interface is used in type binding visiting algorithms.
 *
 * @see Bindings#visitHierarchy(ITypeBinding, TypeBindingVisitor)
 */
public interface TypeBindingVisitor {

	public boolean visit(ITypeBinding node);
}
