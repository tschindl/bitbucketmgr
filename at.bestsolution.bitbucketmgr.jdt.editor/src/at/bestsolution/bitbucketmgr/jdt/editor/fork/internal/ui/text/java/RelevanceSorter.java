package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.text.java;

import java.util.Comparator;

import at.bestsolution.efxclipse.text.jface.contentassist.ICompletionProposal;

/**
 * A relevance based sorter.
 *
 * @since 3.2
 */
public final class RelevanceSorter extends AbstractProposalSorter {

	private final Comparator<ICompletionProposal> fComparator= new CompletionProposalComparator();

	public RelevanceSorter() {
	}

	/*
	 * @see org.eclipse.jdt.ui.text.java.AbstractProposalSorter#compare(org.eclipse.jface.text.contentassist.ICompletionProposal, org.eclipse.jface.text.contentassist.ICompletionProposal)
	 */
	@Override
	public int compare(ICompletionProposal p1, ICompletionProposal p2) {
		return fComparator.compare(p1, p2);
	}
}
