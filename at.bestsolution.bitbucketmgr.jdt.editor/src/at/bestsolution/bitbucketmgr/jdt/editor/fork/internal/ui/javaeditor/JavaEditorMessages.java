package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor;

public class JavaEditorMessages {

	public static final String SemanticHighlighting_job = "";
	public static String SemanticHighlighting_staticFinalField = "";
	public static String SemanticHighlighting_staticField = "";
	public static String SemanticHighlighting_field = "";
	public static String SemanticHighlighting_autoboxing = "";
	public static String SemanticHighlighting_methodDeclaration = "";
	public static String SemanticHighlighting_staticMethodInvocation = "";
	public static String SemanticHighlighting_annotationElementReference = "";
	public static String SemanticHighlighting_abstractMethodInvocation = "";
	public static String SemanticHighlighting_inheritedMethodInvocation = "";
	public static String SemanticHighlighting_method = "";
	public static String SemanticHighlighting_localVariableDeclaration = "";
	public static String SemanticHighlighting_localVariable = "";
	public static String SemanticHighlighting_parameterVariable = "";
	public static String SemanticHighlighting_deprecatedMember = "";
	public static String SemanticHighlighting_typeVariables = "";
	public static String SemanticHighlighting_classes = "";
	public static String SemanticHighlighting_enums = "";
	public static String SemanticHighlighting_interfaces = "";
	public static String SemanticHighlighting_annotations = "";
	public static String SemanticHighlighting_typeArguments = "";
	public static String SemanticHighlighting_numbers = "";
	public static String SemanticHighlighting_abstractClasses = "";
	public static String SemanticHighlighting_inheritedField = "";
	public static String CompilationUnitDocumentProvider_error_saveParticipantSavedFile = "";
	public static String CompilationUnitDocumentProvider_error_saveParticipantFailed = "";
	public static String CompilationUnitDocumentProvider_error_saveParticipantProblem = "";
	public static String CompilationUnitDocumentProvider_progressNotifyingSaveParticipants = "";
	public static String CompilationUnitDocumentProvider_saveAsTargetOpenInEditor = "";
	public static String CompilationUnitDocumentProvider_error_calculatingChangedRegions = "";
	public static String CompilationUnitDocumentProvider_calculatingChangedRegions_message = "";

}
