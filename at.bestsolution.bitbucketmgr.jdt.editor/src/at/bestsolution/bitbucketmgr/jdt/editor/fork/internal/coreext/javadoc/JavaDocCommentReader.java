package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.coreext.javadoc;

import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jdt.core.formatter.IndentManipulation;

import at.bestsolution.efxclipse.text.jface.html.SingleCharReader;


/**
 * Reads a java doc comment from a java doc comment. Skips star-character on begin of line.
 */
public class JavaDocCommentReader extends SingleCharReader {

	private IBuffer fBuffer;

	private int fCurrPos;

	private int fStartPos;

	private int fEndPos;

	private boolean fWasNewLine;

	public JavaDocCommentReader(IBuffer buf, int start, int end) {
		fBuffer= buf;
		fStartPos= start + 3;
		fEndPos= end - 2;

		reset();
	}

	/**
	 * @see java.io.Reader#read()
	 */
	@Override
	public int read() {
		if (fCurrPos < fEndPos) {
			char ch= fBuffer.getChar(fCurrPos++);
			if (fWasNewLine && !IndentManipulation.isLineDelimiterChar(ch)) {
				while (fCurrPos < fEndPos && Character.isWhitespace(ch)) {
					ch= fBuffer.getChar(fCurrPos++);
				}
				if (ch == '*') {
					if (fCurrPos < fEndPos) {
						do {
							ch= fBuffer.getChar(fCurrPos++);
						} while (ch == '*');
					} else {
						return -1;
					}
				}
			}
			fWasNewLine= IndentManipulation.isLineDelimiterChar(ch);

			return ch;
		}
		return -1;
	}

	/**
	 * @see java.io.Reader#close()
	 */
	@Override
	public void close() {
		fBuffer= null;
	}

	/**
	 * @see java.io.Reader#reset()
	 */
	@Override
	public void reset() {
		fCurrPos= fStartPos;
		fWasNewLine= true;
		// skip first line delimiter:
		if (fCurrPos < fEndPos && '\r' == fBuffer.getChar(fCurrPos)) {
			fCurrPos++;
		}
		if (fCurrPos < fEndPos && '\n' == fBuffer.getChar(fCurrPos)) {
			fCurrPos++;
		}
	}


	/**
	 * Returns the offset of the last read character in the passed buffer.
	 * 
	 * @return the offset
	 */
	public int getOffset() {
		return fCurrPos;
	}


}
