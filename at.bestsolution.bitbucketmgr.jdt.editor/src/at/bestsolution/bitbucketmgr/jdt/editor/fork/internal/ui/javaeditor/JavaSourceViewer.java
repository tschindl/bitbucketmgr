package at.bestsolution.bitbucketmgr.jdt.editor.fork.internal.ui.javaeditor;

import java.util.ArrayList;

import org.eclipse.core.runtime.Assert;

import at.bestsolution.efxclipse.text.jface.ITextPresentationListener;
import at.bestsolution.efxclipse.text.jface.source.SourceViewer;

public class JavaSourceViewer extends /*ProjectionViewer*/ SourceViewer {
	/**
	 * Prepends the text presentation listener at the beginning of the viewer's
	 * list of text presentation listeners.  If the listener is already registered
	 * with the viewer this call moves the listener to the beginning of
	 * the list.
	 *
	 * @param listener the text presentation listener
	 * @since 3.0
	 */
	public void prependTextPresentationListener(ITextPresentationListener listener) {

		Assert.isNotNull(listener);

		if (textPresentationListeners == null)
			textPresentationListeners= new ArrayList<ITextPresentationListener>();

		textPresentationListeners.remove(listener);
		textPresentationListeners.add(0, listener);
	}
}
