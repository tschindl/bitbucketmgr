/*******************************************************************************
 * Copyright (c) 2012 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucketmgr.services;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import at.bestsolution.bitbucketmgr.services.AuthentificationService.AuthToken;
import at.bestsolution.bitbucketmgr.services.model.Changeset;
import at.bestsolution.bitbucketmgr.services.model.Issue;
import at.bestsolution.bitbucketmgr.services.model.PagedList;
import at.bestsolution.bitbucketmgr.services.model.Repository;
import at.bestsolution.efxclipse.runtime.core.Callback;

public interface RepositoryService {
	public Repository create(IProgressMonitor monitor, AuthToken authToken, String name, boolean isPrivate);
	public Repository findRepository(IProgressMonitor monitor, AuthToken authToken, String owner, String name);
	public void getIssues(IProgressMonitor monitor, AuthToken authToken, String owner, String projectName, Callback<List<Issue>> success, Callback<IStatus> error);
	public void getChangeset(IProgressMonitor monitor, AuthToken authToken, Repository repository, int pageSize, Callback<PagedList<Changeset>> callback);
}
