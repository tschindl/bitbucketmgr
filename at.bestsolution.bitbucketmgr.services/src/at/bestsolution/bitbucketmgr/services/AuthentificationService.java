/*******************************************************************************
 * Copyright (c) 2012 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucketmgr.services;

import org.eclipse.core.runtime.IProgressMonitor;

import at.bestsolution.efxclipse.runtime.core.Callback;

public interface AuthentificationService {
	public void authentificate(IProgressMonitor monitor, String username, String password, Callback<AuthToken> callback);
	
	public interface AuthToken {
		public String getUsername();
	}
}
