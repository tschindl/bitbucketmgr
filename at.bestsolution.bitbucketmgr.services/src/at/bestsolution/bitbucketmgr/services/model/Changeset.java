package at.bestsolution.bitbucketmgr.services.model;

public interface Changeset {
	public String getDescription();
	public String getCommitter();
}
