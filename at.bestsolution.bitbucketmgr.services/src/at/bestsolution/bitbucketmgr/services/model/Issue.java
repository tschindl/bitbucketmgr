package at.bestsolution.bitbucketmgr.services.model;

public interface Issue {
	public String getTitle();
	public String getDescription();
	public String getStatus();
}
