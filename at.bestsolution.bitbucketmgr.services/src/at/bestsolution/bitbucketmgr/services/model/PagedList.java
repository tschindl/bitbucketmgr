package at.bestsolution.bitbucketmgr.services.model;

import java.util.List;

public interface PagedList<T> {
	public int pages();
	public List<T> getPageList(int page);
}
