package at.bestsolution.bitbucketmgr.services.mock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import at.bestsolution.bitbucketmgr.services.AuthentificationService.AuthToken;
import at.bestsolution.bitbucketmgr.services.RepositoryService;
import at.bestsolution.bitbucketmgr.services.model.Changeset;
import at.bestsolution.bitbucketmgr.services.model.Issue;
import at.bestsolution.bitbucketmgr.services.model.PagedList;
import at.bestsolution.bitbucketmgr.services.model.Repository;
import at.bestsolution.efxclipse.runtime.core.Callback;

public class MockRepositoryService implements RepositoryService {

	@Override
	public Repository create(IProgressMonitor monitor, AuthToken authToken,
			String name, boolean isPrivate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Repository findRepository(IProgressMonitor monitor,
			AuthToken authToken, String owner, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void getIssues(IProgressMonitor monitor,
			AuthToken authToken, String owner, String projectName, Callback<List<Issue>> success, Callback<IStatus> error) {
		List<List<Issue>> lists = new ArrayList<>();
		
		List<Issue> aList = new ArrayList<>();
		aList.add(new IssueImpl("Implement RepositoryService", "wontfix", "Bla Bla Bla\nBlo Blo Blo"));
		aList.add(new IssueImpl("Add license informations", "onhold", "Bla Bla Bla\nBlo Blo Blo"));
		aList.add(new IssueImpl("Add repository issue API", "duplicate", "Bla Bla Bla\nBlo Blo Blo"));
		aList.add(new IssueImpl("Add possibility to publish p2 repo", "resolved", "Bla Bla Bla\nBlo Blo Blo"));
		aList.add(new IssueImpl("Export public API", "new", "Bla Bla Bla\nBlo Blo Blo"));
		aList.add(new IssueImpl("adding build integration", "open", "Bla Bla Bla\nBlo Blo Blo"));
		lists.add(aList);
		
		success.call(Collections.unmodifiableList(aList));
	}
	
	@Override
	public void getChangeset(IProgressMonitor monitor, AuthToken authToken,
			Repository repository, int pageSize,
			Callback<PagedList<Changeset>> callback) {
		List<List<Changeset>> lists = new ArrayList<>();
		List<Changeset> aList = new ArrayList<>();
		
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ssX");
		
		try {
			aList.add(new ChangesetImpl("#7 - fixed segmented list implementation", "Tom Schindl", f.parse("2013-01-11 00:17:00+00:00")));
			aList.add(new ChangesetImpl("#7 - implemented segmented list", "Tom Schindl", f.parse("2013-01-11 00:08:44+00:00")));
			aList.add(new ChangesetImpl("#7 - adding plain list API", "Tom Schindl", f.parse("2013-01-10 23:57:09+00:00")));
			aList.add(new ChangesetImpl("#6 - fixed published version", "Tom Schindl", f.parse("2013-01-08 17:22:43+00:00")));
			aList.add(new ChangesetImpl("#6 - fixed webpath", "Tom Schindl", f.parse("2013-01-08 17:13:57+00:00")));
			aList.add(new ChangesetImpl("#6 - adding build.xml which can be used to publish the repo", "Tom Schindl", f.parse("2013-01-08 17:06:16+00:00")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		callback.call(Util.create(lists));
	}

	static class IssueImpl implements Issue {
		private final String title;
		private final String status;
		private final String description;
		
		public IssueImpl(String title, String status, String description) {
			this.title = title;
			this.status = status;
			this.description = description;
		}
		
		@Override
		public String getTitle() {
			return title;
		}

		@Override
		public String getStatus() {
			return status;
		}
		
		@Override
		public String getDescription() {
			return description;
		}
	}

	static class ChangesetImpl implements Changeset {
		private final String description;
		private final String committer;
		private final Date date;
		
		public ChangesetImpl(String description, String committer, Date date) {
			this.description = description;
			this.committer = committer;
			this.date = date;
		}
		
		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public String getCommitter() {
			return committer;
		}
		
		public Date getDate() {
			return date;
		}
	}
}
