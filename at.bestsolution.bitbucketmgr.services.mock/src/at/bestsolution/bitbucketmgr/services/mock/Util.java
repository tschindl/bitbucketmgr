package at.bestsolution.bitbucketmgr.services.mock;

import java.util.List;

import at.bestsolution.bitbucketmgr.services.model.PagedList;

public class Util {
	public static <T> PagedList<T> create(final List<List<T>> list) {
		return new PagedList<T>() {

			@Override
			public int pages() {
				return list.size();
			}

			@Override
			public List<T> getPageList(int page) {
				return list.get(page);
			}
		};
		
	}
}
