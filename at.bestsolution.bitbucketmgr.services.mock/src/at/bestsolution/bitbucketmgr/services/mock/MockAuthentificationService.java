/*******************************************************************************
 * Copyright (c) 2012 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucketmgr.services.mock;

import org.eclipse.core.runtime.IProgressMonitor;

import at.bestsolution.bitbucketmgr.services.AuthentificationService;
import at.bestsolution.efxclipse.runtime.core.Callback;

public class MockAuthentificationService implements AuthentificationService {

	@Override
	public void authentificate(IProgressMonitor monitor, final String username, String password, Callback<AuthToken> callback) {
		callback.call("john".equals(username) && "git".equals(password) ? new AuthToken() {
			@Override
			public String getUsername() {
				return username;
			}
		} : null);
	}

}
