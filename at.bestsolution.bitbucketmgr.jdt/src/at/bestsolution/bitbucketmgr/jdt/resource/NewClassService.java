package at.bestsolution.bitbucketmgr.jdt.resource;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import at.bestsolution.bitbucketmgr.ui.services.ResourceService;
import at.bestsolution.efxclipse.runtime.dialogs.TitleAreaDialog;

public class NewClassService implements ResourceService {

	@Override
	public String getName() {
		return "New Java Class";
	}

	@Override
	public void createFile(final IFolder container, Stage stage) {
		ClassDialog dialog = new ClassDialog(container, stage);	

		dialog.open();
	}
	
	IResource handleElementCreation(IContainer container, String packageName,
			String className) {
		// FIXME This is temporary
		if (!container.getProject().isOpen()) {
			try {
				container.getProject().open(new NullProgressMonitor());
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		IProject pr = container.getProject();

		IJavaElement jElement = getInitialJavaElement(container);
		IPackageFragmentRoot jRoot = getFragmentRoot(jElement);

		try {
			IJavaProject jProject = JavaCore.create(pr);
			jProject.open(new NullProgressMonitor());
			jRoot.open(new NullProgressMonitor());
			IPackageFragment fragment = jRoot.getPackageFragment(packageName);
			if( ! fragment.exists() ) {
				((IFolder)fragment.getResource()).create(true, true, null);
			}
			ICompilationUnit u = fragment.getCompilationUnit(className + ".java");
			IFile f = (IFile) u.getResource();
			ByteArrayInputStream in = new ByteArrayInputStream(new String("package " + fragment.getElementName()+";\n\n"+
					"public class " + className + " {\n}").getBytes());
			f.create(in, IFile.FORCE | IFile.KEEP_HISTORY,
					new NullProgressMonitor());
			in.close();
			// pr.build(IncrementalProjectBuilder.FULL_BUILD, new
			// NullProgressMonitor());
			return f;
		} catch (JavaModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.err.println(jRoot);

		return null;
	}
	
	private IJavaElement getInitialJavaElement(IContainer container) {
		IJavaElement jelem = null;
		Object selectedElement = container;
		if (selectedElement instanceof IAdaptable) {
			IAdaptable adaptable = (IAdaptable) selectedElement;

			jelem = (IJavaElement) adaptable.getAdapter(IJavaElement.class);
			if (jelem == null || !jelem.exists()) {
				jelem = null;
				IResource resource = (IResource) adaptable
						.getAdapter(IResource.class);
				if (resource != null && resource.getType() != IResource.ROOT) {
					while (jelem == null
							&& resource.getType() != IResource.PROJECT) {
						resource = resource.getParent();
						jelem = (IJavaElement) resource
								.getAdapter(IJavaElement.class);
					}
					if (jelem == null) {
						jelem = JavaCore.create(resource); // java project
					}
				}
			}
		}
		return jelem;
	}
	
	protected IPackageFragmentRoot getFragmentRoot(IJavaElement elem) {
		IPackageFragmentRoot initRoot = null;
		if (elem != null) {
			initRoot = (IPackageFragmentRoot) elem
					.getAncestor(IJavaElement.PACKAGE_FRAGMENT_ROOT);
			try {
				if (initRoot == null
						|| initRoot.getKind() != IPackageFragmentRoot.K_SOURCE) {
					IJavaProject jproject = elem.getJavaProject();
					if (jproject != null) {
						initRoot = null;
						if (jproject.exists()) {
							IPackageFragmentRoot[] roots = jproject
									.getPackageFragmentRoots();
							for (int i = 0; i < roots.length; i++) {
								if (roots[i].getKind() == IPackageFragmentRoot.K_SOURCE) {
									initRoot = roots[i];
									break;
								}
							}
						}
						if (initRoot == null) {
							initRoot = jproject.getPackageFragmentRoot(jproject
									.getResource());
						}
					}
				}
			} catch (JavaModelException e) {
				// TODO
				e.printStackTrace();
			}
		}
		return initRoot;
	}
	
	class ClassDialog extends TitleAreaDialog {		
		private TextField packageName;
		private TextField className;
		private IFolder container;
		
		public ClassDialog(final IFolder container, Stage stage) {
			super(stage, "New Class", "New Class",
				"Create a new Java Class", ClassDialog.class.getClassLoader()
						.getResource("/icons/wizban/newclass_wiz.png"));
			this.container = container;
		}

		@Override
		protected Node createDialogContent() {
			GridPane pane = new GridPane();
			pane.setHgap(10);
			pane.setVgap(5);

			{
				Label l = new Label("Package:");
				pane.add(l, 0, 0);

				packageName = new TextField();
				pane.add(packageName, 1, 0);
				GridPane.setHgrow(packageName, Priority.ALWAYS);
			}

			{
				Label l = new Label("Name:");
				pane.add(l, 0, 1);

				className = new TextField();
				pane.add(className, 1, 1);
				GridPane.setHgrow(className, Priority.ALWAYS);
			}

			return pane;
		}

		@Override
		protected void okPressed() {
			IResource resource = handleElementCreation(container,
					packageName.getText(), className.getText());
			if (resource != null) {
				super.okPressed();
			}
		}
	}
}
