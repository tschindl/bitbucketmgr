package at.bestsolution.bitbucketmgr.services.rest;

import static at.bestsolution.bitbucketmgr.services.rest.Util.error;
import static at.bestsolution.bitbucketmgr.services.rest.Util.monitor;
import static at.bestsolution.bitbucketmgr.services.rest.Util.session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import at.bestsolution.bitbucket.rest.BitbucketRepositoriesService;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue;
import at.bestsolution.bitbucket.rest.util.ResponseObject;
import at.bestsolution.bitbucketmgr.services.AuthentificationService.AuthToken;
import at.bestsolution.bitbucketmgr.services.RepositoryService;
import at.bestsolution.bitbucketmgr.services.model.Changeset;
import at.bestsolution.bitbucketmgr.services.model.Issue;
import at.bestsolution.bitbucketmgr.services.model.PagedList;
import at.bestsolution.bitbucketmgr.services.model.Repository;
import at.bestsolution.bitbucketmgr.services.rest.model.IssueImpl;
import at.bestsolution.efxclipse.runtime.core.Callback;

public class RestRepositoryService implements RepositoryService {

	@Override
	public Repository create(IProgressMonitor monitor, AuthToken authToken,
			String name, boolean isPrivate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Repository findRepository(IProgressMonitor monitor,
			AuthToken authToken, String owner, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void getIssues(IProgressMonitor monitor, AuthToken authToken,
			String owner, String projectName, Callback<List<Issue>> success,
			Callback<IStatus> error) {
		BitbucketRepositoriesService service = session(authToken)
				.createRepositoryService();
		ResponseObject<List<RepIssue>> response = service.getIssues(
				monitor(monitor), owner, projectName);

		if (response.isOk()) {
			success.call(Collections.unmodifiableList(toIssueList(response
					.value())));
		} else {
			error.call(error(response));
		}
	}

	private static List<Issue> toIssueList(List<RepIssue> issues) {
		List<Issue> rv = new ArrayList<>();
		for( RepIssue i : issues ) {
			rv.add(new IssueImpl(i));
		}
		return rv;
	}

	@Override
	public void getChangeset(IProgressMonitor monitor, AuthToken authToken,
			Repository repository, int pageSize,
			Callback<PagedList<Changeset>> callback) {
		// TODO Auto-generated method stub

	}

}
