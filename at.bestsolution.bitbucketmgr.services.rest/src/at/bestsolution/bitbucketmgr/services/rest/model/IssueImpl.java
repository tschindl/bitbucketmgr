package at.bestsolution.bitbucketmgr.services.rest.model;

import at.bestsolution.bitbucket.rest.model.repositories.RepIssue;
import at.bestsolution.bitbucketmgr.services.model.Issue;

public class IssueImpl implements Issue {
	private final RepIssue issue;

	public IssueImpl(RepIssue issue) {
		this.issue = issue;
	}
	
	@Override
	public String getTitle() {
		return issue.getTitle();
	}

	@Override
	public String getDescription() {
		return issue.getContent();
	}

	@Override
	public String getStatus() {
		return issue.getStatus().name().toLowerCase();
	}
}
