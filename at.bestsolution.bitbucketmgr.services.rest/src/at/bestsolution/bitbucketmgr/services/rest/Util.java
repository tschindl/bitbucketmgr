package at.bestsolution.bitbucketmgr.services.rest;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.util.ProgressMonitor;
import at.bestsolution.bitbucket.rest.util.ResponseObject;
import at.bestsolution.bitbucketmgr.services.AuthentificationService.AuthToken;
import at.bestsolution.bitbucketmgr.services.rest.RestAuthentificationService.BitbucketAuthToken;

public class Util {
	public static BitbucketAuthToken token(AuthToken token) {
		return (BitbucketAuthToken) token;
	}
	
	public static BitbucketSession session(AuthToken token) {
		return token(token).getSession();
	}
	
	public static IStatus error(ResponseObject<?> rv) {
		return new Status(IStatus.ERROR, "at.bestsolution.bitbucketmgr.services.rest", rv.getException().getMessage(), rv.getException());
	}
	
	public static ProgressMonitor monitor(final IProgressMonitor monitor) {
		return new ProgressMonitor() {
			
			@Override
			public void setTaskname(String task, int percentage) {
				monitor.setTaskName(task);
				monitor.worked(percentage);
			}
		};
	}
}
