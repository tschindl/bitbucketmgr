package at.bestsolution.bitbucketmgr.services.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;

import at.bestsolution.bitbucket.rest.BitbucketService;
import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.util.ProgressMonitor;
import at.bestsolution.bitbucketmgr.services.AuthentificationService;
import at.bestsolution.efxclipse.runtime.core.Callback;

public class RestAuthentificationService implements AuthentificationService {
	private Map<String[], BitbucketAuthToken> sessions = new HashMap<>();
	private BitbucketService service;
	
	public void setBitbucketService(BitbucketService service) {
		this.service = service;
		for( Entry<String[], BitbucketAuthToken> e : sessions.entrySet() ) {
			e.getValue().setSession(service.createSession(ProgressMonitor.NULLMONITOR, e.getKey()[0], e.getKey()[1]));
		}
	}
	
	public void unsetBitbucketService(BitbucketService service) {
		for( BitbucketAuthToken t : sessions.values() ) {
			t.session = null;
		}
	}
	
	@Override
	public void authentificate(IProgressMonitor monitor, String username,
			String password, Callback<AuthToken> callback) {
		BitbucketSession s = service.createSession(ProgressMonitor.NULLMONITOR, username, password);
		BitbucketAuthToken t = null;
		if( s != null ) {
			t = new BitbucketAuthToken(s, username);
		}
		callback.call(t);
	}
	
	public static class BitbucketAuthToken implements AuthToken {
		private BitbucketSession session;
		private String username;
		
		public BitbucketAuthToken(BitbucketSession session, String username) {
			this.session = session;
			this.username = username;
		}
		
		public synchronized BitbucketSession getSession() {
			return session;
		}
		
		public synchronized void setSession(BitbucketSession session) {
			this.session = session;
		}
		
		@Override
		public String getUsername() {
			return username;
		}
	}
}